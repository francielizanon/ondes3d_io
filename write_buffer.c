/* write_buffer.c
 * Author: Francieli Zanon Boito
 * this file provides the implementation of the software layer used to stage and aggregate requests to files
 * this optimization is discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 */

#include "write_buffer.h"
#include "surface_output.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_write_buffer(struct write_buffer_t *write_buffer)
{
	write_buffer->buf = NULL;
	write_buffer->index = 0;
	write_buffer->size = 0;
}

int allocate_write_buffer(struct write_buffer_t *write_buffer, int new_size)
{
	if(new_size <= 0)
		return -1;
	write_buffer->buf = malloc(new_size);
	if(!write_buffer->buf)
		return -1;
	write_buffer->size = new_size;
	return 0;
}

//will either write req to fp if req is larger than the buffer, or buffer it. This could result in flushing the buffer to the file if it gets full
void buffer_to_file(FILE *fp, struct write_buffer_t *write_buffer, char *req, int req_size)
{
	if(req_size >= write_buffer->size) //we are trying to write something larger than the buffer, no need to go through buffer
		fwrite(req, req_size, 1, fp);
	else
	{ 
		if(req_size + write_buffer->index > write_buffer->size) //we don't have any more space on the buffer, let's flush it before proceeding
			flush_buffer_to_file(fp, write_buffer);
		//now we can buffer the request
		memcpy(write_buffer->buf+(write_buffer->index), req, req_size);
		write_buffer->index += req_size;
	}
}
void flush_buffer_to_file(FILE *fp, struct write_buffer_t *write_buffer)
{
	if((write_buffer->size <= 0) || (write_buffer->index <= 0))
		return;
	if (fwrite(write_buffer->buf, write_buffer->index, 1, fp) != 1)
		printf("Warning: could not fwrite buffer to file?\n");
	write_buffer->index = 0;
}
void write_float(FILE *fp, float val, struct write_buffer_t *write_buffer)
{
    force_big_endian((unsigned char *) &val);
    buffer_to_file(fp, write_buffer, (char *) &val, sizeof(float));
}
void cleanup_write_buffer(struct write_buffer_t *write_buffer)
{
	if(write_buffer->buf)
		free(write_buffer->buf);
}
