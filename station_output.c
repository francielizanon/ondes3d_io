/* station_output.c
 * Author: Francieli Zanon Boito
 * this file provides the three available implementation of the station files output routine
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 * options are: "none" (the original implementation), "parallel" (in the paper called "distributed station output") and "MPIIO"
 */

#include <mpi.h>
#include <string.h>
#include <assert.h>
#include "station_output.h"
#include "IO.h"
#include "mpiio_common.h"
#include "main.h"

//--------------------------------------------------------------------------------------------------------------
//this is the original implementation of the station file output
void write_station_files_nooptimization(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, MPI_Comm comm2d, int l, struct write_buffer_t *write_buffer, char *local_buf)
{
	int field, j;
	MPI_Request sendreq;
	MPI_Status status;
	char flname[80]; 
  int fStatus= EXIT_FAILURE ; 	/* status of functions */
			  //everyone sends data to rank 0, who will write everything
			  for ( field = 1; field <=9; field++ ){ /* vx, vy, vz, tii, txy, txz, tyz */
		            	if ( PRM->me == OUT->mapping_seis[ir][field] && PRM->me != 0 ){ //I'm responsible for this data and I'm not rank 0
					//make a buffer with this process' data
			              	for ( j = 0 ; j < PRM->statout ; j++){ /* each timestep being written */
               					OUT->seis_buff[j]=OUT->seis_output[j][ir][field];
              				}
					//send to rank 0
			                MPI_Isend (OUT->seis_buff, PRM->statout, MPI_DOUBLE, 0, 200+field, comm2d, &sendreq);
			                MPI_Wait (&sendreq, &status) ;
            			} /* end sync and send */
			        if ( PRM->me == 0 && OUT->mapping_seis[ir][field] != 0 ){ //I'm NOT responsible for this data and I'm rank 0 (so I need to have it) 
              				MPI_Recv (OUT->seis_buff, PRM->statout, MPI_DOUBLE, OUT->mapping_seis[ir][field], 200+field, comm2d, &status);
              				for ( j = 0 ; j < PRM->statout ; j++){ /* each time */
				                OUT->seis_output[j][ir][field] = OUT->seis_buff[j] ;
              				}
            		  	} /* end receive and sync */
		          } /* end for field */
			  //now rank 0 writes this file
			  if ( PRM->me == 0 ){
				flname[0] = '\0';
				sprintf(flname, "%sobs%d.dat", PRM->dir, ir+1);
				fStatus=OutSeismograms(*OUT,*PRM,ir,l,flname,PRM->statout, local_buf, write_buffer);
			        VerifFunction(fStatus, "write seismogram",*PRM);
		          } /* end of PRM->me = 0 */
}
//--------------------------------------------------------------------------------------------------------------
void write_station_files_mpiio(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, MPI_Comm comm2d, char *local_buf, char *mpiio_station_buffer, int l)
{
	int color, new_rank, new_size, err, resultlen, my_field, j, starting_offset, entry, entry_length=-1;
	MPI_Comm dat_comm;
	char flname[80];
	MPI_File fh;
	char msg[256];
	int header_size;
	MPI_Datatype my_datatype;
	int limit, previous_data;
	int leftover=0;
	int my_size;
	int first = 1;


			  //check if I'm involved in this station file and split the MPI communicator, only the ones that are involved (color == 1) will execute the rest
   			  color = am_i_involved_in_dat(OUT, ir, PRM->me);
			  MPI_Comm_split(comm2d, color, PRM->me, &dat_comm);
		
			  if(color == 1) //if I'm involved in this file
			  {
				//get my rank inside the communicator of processes writing this file
				MPI_Comm_rank(dat_comm, &new_rank);
				MPI_Comm_size(dat_comm, &new_size);
				//open the file
				flname[0] = '\0';
				sprintf(flname, "%sobs%d.dat", PRM->dir, ir+1);
				err = MPI_File_open(dat_comm, flname,  MPI_MODE_WRONLY | MPI_MODE_CREATE ,MPI_INFO_NULL,&fh);
				if(err != MPI_SUCCESS)
				{
					MPI_Error_string(err, msg, &resultlen);
					printf("Could not open the file %s\n%s\n", flname, msg);
					return;
				}
				//rank 0 (from the new communicator) will write the header
				if(new_rank == 0)
				{
					local_buf[0]='\0';
					sprintf(local_buf, "%d %f %f %f\n%d %f\n", OUT->nobs[ir], OUT->xobs[ir], OUT->yobs[ir], OUT->zobs[ir], l, PRM->dt);
					header_size = strlen(local_buf);
					err = MPI_File_write_at(fh, 0, local_buf, header_size, MPI_CHAR, MPI_STATUS_IGNORE);
					if(err != MPI_SUCCESS)
					{
						MPI_Error_string(err, msg, &resultlen);
						printf("could not write header to file %s\n%s\n", flname, msg);
						MPI_File_close(&fh);
						return;
					}
				}
  				//now everyone needs to know the header size
				MPI_Bcast(&header_size, 1, MPI_INT, 0, dat_comm);
				//now everyone will write to the file
				//it's a text file where each line corresponds to a timestep. Each line has 9 values (written as %15e), one from each process (one for each field), separated by " " with a "\n" at the end
				// find out how many lines we are going to write
				if(l == PRM->tMax) //we are writing because this is the last timestep
					limit = PRM->tMax % PRM->statout;
				else
					limit = PRM->statout;
				do {
					//find out what parts of the file are mine
					if(first == 0)
					{
						assert(my_field + my_size <= 9);
						leftover = get_my_dat_field(OUT, ir, PRM->me, my_field + my_size, &my_size, &my_field);
						assert(my_size > 0);
					}
					else 
						leftover = get_my_dat_field(OUT, ir, PRM->me, 1, &my_size, &my_field);
					
					
					//first I'll copy my data to a buffer
					mpiio_station_buffer[0] = '\0';
					for(j=0; j < limit; j++)
					{
						for(entry = my_field; entry < my_field + my_size; entry++)
						{
							flname[0]='\0';
							sprintf(flname, "%15e", OUT->seis_output[j][ir][entry]);
							strcat(mpiio_station_buffer, flname);
							if(entry == 9)
								strcat(mpiio_station_buffer, "\n");
							else
								strcat(mpiio_station_buffer, " ");
							if(entry_length == -1)
							{
								entry_length = strlen(flname)+1;
							}
						}
					}
					assert(entry_length > 0);
					//create a datatype to describe each contiguous portion of data and the distance between them (here the contiguous portion is a "%15e " or "%15e\n")
					if(!Create_new_output_datatype(limit, my_size*entry_length, 9*entry_length, &my_datatype, MPI_CHAR)) //TODO is the displacemnet correct? "number of elements between start of each block)
					{
						MPI_File_close(&fh);
						return;
					}
					//define a file view which is formed by a defined datatype, starting at a given offset
					starting_offset = (my_field-1)*entry_length;
					previous_data = (l - limit)*9*entry_length;
					err = MPI_File_set_view(fh, header_size+previous_data+starting_offset, MPI_CHAR, my_datatype, "native", MPI_INFO_NULL);
					if(err != MPI_SUCCESS)
					{
						MPI_Error_string(err, msg, &resultlen);
						printf("Could not create file view for file %s\n%s\n", flname, msg);
						MPI_File_close(&fh);
						MPI_Type_free(&my_datatype);
						return;
					} 
					//write
					if(first)
						err = MPI_File_write_all(fh, mpiio_station_buffer, limit*entry_length*my_size, MPI_CHAR, MPI_STATUS_IGNORE);
					else
						err = MPI_File_write(fh, mpiio_station_buffer, limit*entry_length*my_size, MPI_CHAR, MPI_STATUS_IGNORE);
					if(err != MPI_SUCCESS)
					{
						MPI_Error_string(err, msg, &resultlen);
						printf("Could not write to file %s\n%s\n", flname, msg);
					} 
					MPI_Type_free(&my_datatype);
					first =0;
				} while(leftover > 0);
				//cleanup
				MPI_File_close(&fh);
			} //end if I'm involved in this file
			MPI_Comm_free(&dat_comm);
}
//--------------------------------------------------------------------------------------------------------------
//this version is referred to at the PDP paper as "distributed station output"
void write_station_files_parallel_communicationstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int *writer_rank, int *files_per_process, int ir, MPI_Comm comm2d)
{
	int field, j;
	MPI_Request sendreq;
	MPI_Status status;
	  		  //optimization 1 we will distribute the work of writing the station files among all processes
			  //we'll start by deciding who should write the file
			  writer_rank[ir] = get_dat_writer_rank(OUT, ir, files_per_process);
			  files_per_process[writer_rank[ir]]++;
				
			  //now all 9 processes will transfer their data to the writer one (actually 8 processes because the writer rank does not need to transfer data to itself)
			  for ( field = 1; field <=9; field++ ){ /* vx, vy, vz, tii, txy, txz, tyz */
				if ( PRM->me == OUT->mapping_seis[ir][field] && PRM->me != writer_rank[ir] ) //if I'm involved in this file and I'm not the writer rank
				{   
					//make a buffer with this process' data
					for ( j = 0 ; j < PRM->statout ; j++){ /* each time */
						OUT->seis_buff[j]=OUT->seis_output[j][ir][field];
					}
					//send to the writer rank
					MPI_Isend (OUT->seis_buff, PRM->statout, MPI_DOUBLE, writer_rank[ir], 200+field, comm2d, &sendreq);
					MPI_Wait (&sendreq, &status) ;
				} /* end sync and send */
				if( (PRM->me == writer_rank[ir]) && (OUT->mapping_seis[ir][field] != writer_rank[ir])) //if I'm the writer rank and I'm not the one responsible for this portion of data
				{
					//receive data from the process
					MPI_Recv (OUT->seis_buff, PRM->statout, MPI_DOUBLE, OUT->mapping_seis[ir][field], 200+field, comm2d, &status);
					//copy data to my local data structure which will be sent to the file
					for ( j = 0 ; j < PRM->statout ; j++){ /* each time */
						OUT->seis_output[j][ir][field] = OUT->seis_buff[j] ;
					}
				} /* end receive and sync */
			} /* end for field */
      	  		//optimization 2: we'll separate in two phases: one for exchanging data and another to write the files (so a process involved in writing a file won't slow down the communication part of the next file it is involved in) <- that is why we won't write now inside this loop for each file
}
void write_station_files_parallel_IOstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, int l, char *local_buf, struct write_buffer_t *write_buffer)
{
	char flname[80];
  int fStatus= EXIT_FAILURE ; 	/* status of functions */
	flname[0] = '\0';
	sprintf(flname, "%sobs%d.dat", PRM->dir, ir+1);
  	fStatus=OutSeismograms(*OUT,*PRM,ir,l,flname,PRM->statout, local_buf, write_buffer);
					VerifFunction(fStatus, "write seismogramm",*PRM);
}
//--------------------------------------------------------------------------------------------------------------
//functions to help determine who will write each file (for the parallel optimization), to check if a process is involved in a file and to get what field will be provided by that process
int get_dat_writer_rank(struct OUTPUTS *OUT, int ir, int *files_per_process)
{
	int field;
	int writer_rank = OUT->mapping_seis[ir][9];
	for(field = 1; field <= 9; field++) //we select one of the 9 ranks involved in this file
	{
		if(files_per_process[OUT->mapping_seis[ir][field]] == 0) //we get the first rank which is not already writing a file
		{
			writer_rank = OUT->mapping_seis[ir][field];
			break;
		}
		else if(files_per_process[writer_rank] > files_per_process[OUT->mapping_seis[ir][field]]) //if all of them are writing a file, we get the one with less files to write
			writer_rank = OUT->mapping_seis[ir][field];
	}
	return writer_rank;
}
int am_i_involved_in_dat(struct OUTPUTS *OUT, int ir, int me)
{
  int field;
  int ret=0;
  for(field = 1; field <=9; field++)
  {
    if(me == OUT->mapping_seis[ir][field])
    {
      ret = 1;
      break;
    } 
  }
  return ret;
}
//the station (dat) file has 9 entries per line. This function looks to say which ones of those belong to PRM->me
//it sets start to the first one of them and size to the number of consecutive entries
//it is possible that a single process has non-contiguous subsets of the entries. If that is the case, the function will return 1 to signal another write operation has to be done to finish writing the data
//first_field must be set to 1 in the first call, and then it will be used to check there are no more leftovers.
int get_my_dat_field(struct OUTPUTS *OUT, int ir, int me, int first_field, int *size, int *start)
{
	int field=-1;
	*size = 0;
	*start = -1;
	int ended=0;
	int left=0;
	for(field = first_field; field <= 9; field++)
	{
    		if(me == OUT->mapping_seis[ir][field])
		{
			if(ended == 1) //we already found a contiguous portion and now we are looking at another
			{
				left = 1;
				break;
			}
			*size = *size + 1;
			if(*start == -1)
				*start = field;
		}
		else if(*size > 0) //we already read a contiguous portion
			ended = 1;
	}
	return left;
}
