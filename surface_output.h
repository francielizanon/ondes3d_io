/* surface_output.h
 * Author: Francieli Zanon Boito
 * this file provides headers for the three available implementation of the surface files output routine
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 * options are: "none" (the original implementation), "parallel" (in the paper called "distributed surface output") and "MPIIO"
 */

#ifndef _SURFACE_OUTPUT_H_
#define _SURFACE_OUTPUT_H_

#include "write_buffer.h"
#include "struct.h"

#include <mpi.h>

struct xyzbuf_t {
	double **xx; //xy files
	double **xy;
	double **xz;
	double **yx; //yz files
	double **yy;
	double **yz;
	double **zx; //xz files
	double **zy;
	double **zz;
};

void write_surface_files_nooptimization(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int np, int **coords_global, MPI_Comm comm2d, int l);
void write_surface_files_mpiio(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int x, int y, int np, char *mpiio_surface_buffer, MPI_Comm comm2d, int l);
void write_surface_files_parallel_communicationstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, int writer_xy, int writer_xz, int writer_yz, struct xyzbuf_t *xyzbuf, int np, int **coords_global, int l, MPI_Comm comm2d);
void write_surface_files_parallel_IOstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int writer_xy, int writer_xz, int writer_yz, struct xyzbuf_t *xyzbuf, int np, int l);
void setup_write_vtk_indexes(struct PARAMETERS *PRM, int x, int y, int MPMX, int MPMY, int *first_i, int *first_j, int *max_i, int *max_j);
void restore_write_vtk_indexes(struct PARAMETERS *PRM);
void force_big_endian(unsigned char *bytes);
#endif
