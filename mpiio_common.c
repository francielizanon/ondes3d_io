/* mpiio_common.c
 * Author: Francieli Zanon Boito
 * this file provides auxiliary functions for the "MPIIO" implementations of station and surface output routines 
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 */


#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "mpiio_common.h"

//creates and commits new MPI_Datatype to write non-contiguous portions to a file, checks for errors and returns 0 if it fails
int Create_new_output_datatype(int count, int len, int distance, MPI_Datatype *my_datatype, MPI_Datatype basic_type)
{
	int err, resultlen;
	char msg[256];

	err = MPI_Type_vector(count, len, distance, basic_type, my_datatype);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		printf("Panic! could not define datatype to write vtk file\n%s\n", msg);
		return 0;
	}
	err = MPI_Type_commit(my_datatype);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		printf("Panic! could not commit datatype to write vtk file\n%s\n", msg);
		return 0;
	}
	return 1;
}
