//* ONDES3D : FDM code for seismic wave propagation */
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>

#ifdef MPI 
	#include <mpi.h>
#endif
#ifdef OMP
        #include <omp.h>
#endif


#include "struct.h"
#include "options.h"

#include "nrutil.h"
#include "inlineFunctions.h"

#include "computeVeloAndSource.h"
#include "computeStress.h"
#include "computeIntermediates.h"
#include "IO.h"
#include "alloAndInit.h"
#include "main.h"
#include "surface_output.h"
#include "write_buffer.h"
#include "station_output.h"

#if defined (PROFILE1) || defined (PROFILE2)
#include <tau.h>
#endif

#if defined (MISS) || defined (FLOPS)
#include "papi.h"
#endif


#define HOGE "hoge"

#ifdef TOPO_MPI
static const char TOPOFILE[50]= "topologie.in";
#else
static const char TOPOFILE[50]= QUOTEME(TOPOLOGIE);
#endif

static const char BENCHFILE[50]= "NICE-OUTPUT/bench.out";

/* OpenMP */
// Position provisoire - a integrer a la structure PRM
int numThreads_Level_1;


/* Beginning of main */
int main ( int argc, char **argv )
{/* Model parameters */
  struct PARAMETERS PRM={0};
  /* Source : values */
  struct SOURCE SRC={0};

  /* Velocity and stress */
  struct VELOCITY v0={0};
  struct STRESS   t0={0};
  /* Medium */
  struct MEDIUM MDM={0};

  /* Variables for the PML/CPML */
  struct ABSORBING_BOUNDARY_CONDITION ABC={0};

  /* ANELASTICITIES */
  struct ANELASTICITY ANL={0};

  /* OUTPUTS */
  struct OUTPUTS OUT={0};

  /* mapping */
  double  DS, DT; 		/* parameters PRM */
  int    TMAX;
  int   XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX;
  int  ZMAX0,DELTA;
  int MPMX, MPMY;
  int ME;

  /* Other variables */
  int fStatus= EXIT_FAILURE ; 	/* status of functions */
  int     i, j, k, k2, l, l1, ind, ind1, indx, indy, indz;
  double  time;
  FILE *fp5;

  /* MPI variables */
#ifdef MPI  
  char	pname[MPI_MAX_PROCESSOR_NAME];
  MPI_Comm comm2d, rc;
#endif
 
  //write buffer 
  struct write_buffer_t write_buffer;
  init_write_buffer(&write_buffer);
  char *local_buf = malloc(sizeof(char)*500);
  if(!local_buf)
  {
	fprintf(stderr, "Error: cannot allocate memory\n");
	exit(-1);
  }

  /* Variables for the communication between the CPUs */
  struct COMM_DIRECTION NORTH={0}, SOUTH={0}, EAST={0}, WEST={0} ;
  /* 1 - direction : N->S
   * 2 - direction : S->N
   * 3 - direction : E->W
   * 4 - direction : W->E
   */
   
  int     nnorth, nsouth, neast, nwest;
 #ifdef MPI
  MPI_Status status[5];
  MPI_Request req[5];  

#if (COMM==1)
  /* WARNING :
   * Originally, optimisations were made
   * by assuming that matrices with 3 indexes :
   * [x][y][z] are contiguous in one direction for some planes.
   * But this depends on "d3tensor" in nrutil.c and of the compiler.
   * So I think, it is kind of machine dependent optimisation but I might be wrong.
   * That's why I decided to drop this optimisation for the moment.
   * Please feel free to implement/try it.
   */
  /* Notations :
   * S at the end indicates Send
   * R at the end indicates Receive
   *
   * 1 - direction : N->S
   * 2 - direction : S->N
   * 3 - direction : E->W
   * 4 - direction : W->E
   */
  MPI_Request reqV0R[5]; 	/* velocity */
  MPI_Request reqT0R[5]; 	/* stress */
  MPI_Request reqKsiR[5]; 	/* ksil
							   (kristek & moczo anelasticity method) */

  MPI_Request reqV0S[5];
  MPI_Request reqT0S[5];
  MPI_Request reqKsiS[5];

  MPI_Status statV0S[5];
  MPI_Status statT0S[5];
  MPI_Status statKsiS[5];

  MPI_Status statV0R[5];
  MPI_Status statT0R[5];
  MPI_Status statKsiR[5];
#endif	/* PERSISTANT */

#endif /*MPI*/

  int     np, resultlength, imp, jmp, imp_tmp, jmp_tmp, icpu, jcpu, imode;
  int     mpmx_begin, mpmx_end;
  int     mpmy_begin, mpmy_end;

  /* for others communications (seismograms & surface"ij" ) */
  
  
#ifdef MPI 
  MPI_Request sendreq[20];
#endif

  int *coords_global[2];
  int     i1, i2;
  int     ir;
  double  w1,w2,w3, w12, w22, w32;
  int other_rank;
  int writer_xy[2];
  int writer_xz[2];
  int writer_yz[2];
  struct xyzbuf_t *parallel_surface_buf[2];
  parallel_surface_buf[0] = malloc(sizeof(struct xyzbuf_t));
  parallel_surface_buf[1] = malloc(sizeof(struct xyzbuf_t));
  if((!parallel_surface_buf[0]) || (!parallel_surface_buf[1]))
  {
	fprintf(stderr, "Error: could not allocate memory\n");
	exit(-1);
  }


  /* PAPI */

#if defined (MISS) || (FLOPS)
  float   real_time, proc_time, mflops;
  long_long flpops;
  double  perc;
  float   ireal_time, iproc_time, imflops;
  long_long iflpops;
  int     retval;
  int     EventSet = PAPI_NULL;
  int     EventCode;
  long_long values[3];
#endif

  /* Timing */

  double  timing1, timing2, timing3, timing4, timing_comm1, timing_comm2;
  double  timing_bc1, timing_bc2, timing_total, timing_sum1, timing_sum2;
  double  timing_bc1_max, timing_bc1_min, timing_bc2_min, timing_bc2_max;
  double  timing_pml, timing_DS4, timing_dt4;
  double time_before_output;

  double  timing_bc_max, timing_bc_min, timing_comm_min, timing_comm_max, timing_bc_total, timing_comm_total;

  double timing_IO_station, timing_IO_surface;						//to save total IO time
  double timingStart, timingEnd;		//marks start and end of a phase



  /* variables for output routines*/
  int color;
  int element_size;
  int err, resultlen;
  int field;
  int first_i, first_j, max_i, max_j;
  int my_field;
  int new_rank;
  int offset_diff;
  int size_mpiio_surface_buf = 0;
  int x,y;
  int z_size;
  char *mpiio_station_buffer=NULL;
  char *mpiio_surface_buffer=NULL;
  char msg[256];
  MPI_File fh;
  MPI_Datatype my_datatype;
  int starting_offset;

  //just some values to avoid calculating offsets everytime
  int three_floats = 3*sizeof(float);
  int one_float = sizeof(float);
  int two_floats = 2*sizeof(float);

  /* ========================== */
  /* Beginning of the PROGRAM   */
  /* ========================== */

  /* Initialization for MPI */
  PRM.me=0;
  np = 1;

#ifdef MPI
  MPI_Init (&argc, &argv);
  MPI_Comm_size (MPI_COMM_WORLD, &np);

  MPI_Comm_dup (MPI_COMM_WORLD, &comm2d);
  MPI_Comm_rank (comm2d, &(ME));
  PRM.me=ME;
#endif

#ifdef OMP

  if (argc<2)
    {
      printf("ondes3d numThreads_OMP\n");
      exit(-1);
    }
  numThreads_Level_1 = atoi(argv[1]);
  printf("Number of threads of OMP is %d Threads\n", numThreads_Level_1);
  omp_set_num_threads(numThreads_Level_1);

#endif

PRM.px=1;
PRM.py=1;
#ifdef MPI
  VerifFunction( ReadTopo(&PRM, TOPOFILE, np), "read topology file", PRM );
#endif

  PRM.coords[1] = PRM.me/PRM.px;
  PRM.coords[0] = PRM.me - PRM.coords[1]*PRM.px;
  nnorth = PRM.me + PRM.px;
  nsouth = PRM.me - PRM.px;
  neast = PRM.me + 1;
  nwest = PRM.me - 1;
  if ( PRM.coords[1] == 0 ) { nsouth = -1 ; }
  if ( PRM.coords[1] == PRM.py-1 ) {nnorth = -1 ; }
  if ( PRM.coords[0] == 0 ) { nwest = -1 ;}
  if ( PRM.coords[0] == PRM.px-1 ) { neast = -1; }





#ifdef MPI
  MPI_Get_processor_name (pname, &resultlength);
  MPI_Barrier (comm2d);
#endif


#if ( VERBOSE > 2 )
  if ( PRM.me == 0 ) {printf("Topology\n"); }
  printf("me:%i - est:%i - ouest:%i - nord:%i - sud:%i \n", PRM.me, neast, nwest, nnorth, nsouth);
  printf("me:%i - coords(0):%i - coords(1):%i \n", PRM.me, PRM.coords[0], PRM.coords[1]);
#endif



  /* =========================== */
  /* Beginning of Initializations */
  /* =========================== */

  timing_comm1 = 0.;
  timing_comm2 = 0.;
  timing_bc1 = 0.;
  timing_bc2 = 0.;
  timing_total = 0.;
  timing_pml = 0.;
  timing_dt4 = 0.;
  timing_DS4 = 0.;
  timing_IO_station = 0.;
  timing_IO_surface = 0.;

  /* TAU */

#ifdef PROFILE1
  TAU_PROFILE_TIMER (pml_sig, "pml_sig", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (free_surface_sig, "free_surface_sig", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (interior_sig, "interior_sig", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (exchange_sig, "exchange_sig", "void (int,char **)", TAU_USER);

  TAU_PROFILE_TIMER (pml_vit, "pml_vit", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (free_surface_vit, "free_surface_vit", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (interior_vit, "interior_vit", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (exchange_vit, "exchange_vit", "void (int,char **)", TAU_USER);

  TAU_PROFILE_SET_NODE (PRM.me)
#endif

#ifdef PROFILE2
  TAU_PROFILE_TIMER (compute_sig, "compute_sig", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (exchange_sig, "exchange_sig", "void (int,char **)", TAU_USER);

  TAU_PROFILE_TIMER (compute_vit, "compute_vit", "void (int,char **)", TAU_USER);
  TAU_PROFILE_TIMER (exchange_vit, "exchange_vit", "void (int,char **)", TAU_USER);

  TAU_PROFILE_SET_NODE (PRM.me)
#endif

#ifdef MISS
    /* PAPI */
  if ( (retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT )
      printf ("ERROR Init \n");
  if ( (retval = PAPI_create_eventset(&EventSet)) != PAPI_OK )
    printf ("ERROR create \n");
  if ( (retval = PAPI_add_event(EventSet, PAPI_L3_DCA)) != PAPI_OK )
    printf ("ERROR add \n");
  if ( (retval = PAPI_add_event(EventSet, PAPI_L3_DCH)) != PAPI_OK )
    printf ("ERROR add \n");
  if ( (retval = PAPI_add_event(EventSet, PAPI_TOT_CYC)) != PAPI_OK )
    printf ("ERROR add \n");

#endif

#ifdef MPI
  MPI_Barrier (comm2d);
#endif 

  /* Read files */
  fStatus= ReadPrmFile( &PRM, &MDM, &ABC, &ANL, &OUT, PRMFILE);
  VerifFunction( fStatus,"read parameter file ", PRM);
 
  //allocate file write buffer   
  if(PRM.bufferSize > 0)
  {
	if(allocate_write_buffer(&write_buffer, PRM.bufferSize) != 0)
	{
		fprintf(stderr, "Error: Could not allocate buffer for file write! Will continue without it.\n");
		PRM.bufferSize = 0;
	}
  }
  //allocate coords_global which is used to write surface files
  if(!strcmp("parallel", PRM.optSurface)){
	  //we'll initialize some things here to save time when writing vtk files
   	coords_global[0] = malloc(sizeof(int)*np);
  	coords_global[1] = malloc(sizeof(int)*np);
  	for(other_rank == 0; other_rank < np; other_rank++)
  	{
  		coords_global[1][other_rank] = other_rank/PRM.px;
  		coords_global[0][other_rank] = other_rank - coords_global[1][other_rank]*PRM.px;
  	}
  }
  else if(!strcmp("none", PRM.optSurface)){
	coords_global[0] = malloc(sizeof(int)*1024);
	coords_global[1] = malloc(sizeof(int)*1024);
  }
  else
  {
	//it is not used by the mpiio implementation, but we need to set it to NULL otherwise we'll try to call the free() function at he end
	coords_global[0] = NULL;
	coords_global[1] = NULL;
  }
  //the mpiio surface output version does not use coords_global
  // allocate a structure for parallel station output, so we can keep track of how many files each process is writing
  int *files_per_process = NULL;
  if(!strcmp("parallel", PRM.optStation))
  {
	files_per_process = malloc(sizeof(int)*np);
	if(!files_per_process)
	{
		fprintf(stderr, "Error: could not allocate a data structure for the parallel station output. Will use no optimization for these files instead\n");
		PRM.optStation[0] = '\0';
		sprintf(PRM.optStation, "none");
	}
  }
  else if(!strcmp("MPIIO", PRM.optStation))
  {
	//need to allocate a buffer for each process' data
	mpiio_station_buffer = malloc(PRM.statout * 300 * sizeof(char));
	if(!mpiio_station_buffer)
	{
		fprintf(stderr, "Error: could not allocate a buffer for the MPI-IO station output routines. Will use no optimization for these files instead\n");
		PRM.optStation[0] = '\0';
		sprintf(PRM.optStation, "none");
	}
  }
	
  fStatus=ReadSrc( &SRC, PRM );
  VerifFunction( fStatus,"read sources file ", PRM);

  /* MPI */
  VerifFunction( InitPartDomain( &PRM, &OUT ),"split domain MPI",PRM );
  

#ifdef MPI
  fStatus = InitializeCOMM( &NORTH, &SOUTH, &EAST, &WEST, /* outputs */
							nnorth, nsouth, neast, nwest,  /* inputs */
							PRM );
  
  VerifFunction( fStatus, "initialize COMMUNICATIONS ",PRM );
#endif


  /* Read others files ( dependent to MPI partionning ) */
  if ( model == GEOLOGICAL )
    VerifFunction( ReadGeoFile( &MDM, PRM ),"read geological file",PRM);
  
  fStatus=ReadStation( &OUT, PRM,MDM);
  VerifFunction( fStatus,"read station file ",PRM);
  //for parallel station output
  int *writer_rank = NULL;
  if(!strcmp("parallel", PRM.optStation))
  {
	writer_rank = malloc(sizeof(int)*OUT.iObs);
	if(!writer_rank)
	{
		fprintf(stderr, "Error: could not allocate data structure for the parallel station output optimization, will use no optimization instead.\n");
		PRM.optStation[0]='\0';
		sprintf(PRM.optStation, "none");
		if(files_per_process)
			free(files_per_process);
	}
  }

  /* allocate fields */
  fStatus= AllocateFields( &v0,&t0,&ANL, &ABC, &MDM, &SRC, /* outputs */
						   PRM  /* inputs */
                           );  
  VerifFunction( fStatus, "allocate Fields ",PRM);


  /** initialize fields **/
  /* init layers */
  if ( model == LAYER ){
    VerifFunction( InitLayerModel( &MDM,&ANL, PRM ),
                   "initialize layer model", PRM);
  }

  /* inside the domain */
  if ( ANLmethod == DAYandBRADLEY ){
    fStatus = InitializeDayBradley( &MDM, &ANL, PRM  );
    VerifFunction( fStatus , "initilize Day and BRADLEY",PRM);
  } else if ( ANLmethod == KRISTEKandMOCZO ){
    fStatus =  InitializeKManelas ( &ANL, &MDM, PRM.dt );
    VerifFunction( fStatus, "initilize KRISTEK and MOCZO anelasticity",PRM);
  }

  /* in the absorbing layers */
  fStatus = InitializeABC( &ABC, &MDM, &ANL, PRM ) ;
  VerifFunction( fStatus, "initilize absorbing boundaries ",PRM);

  if ( model == GEOLOGICAL ){
#ifdef OUT_HOGE
    /* Checking the geological model : we write in a binary file */
    VerifFunction( OutGeol( MDM, OUT, PRM, HOGE, &write_buffer, "check geological model", PRM ); 
#endif
    /* Computing the height of the stations */
    VerifFunction( InitializeGeol(&OUT, MDM, PRM ) , "initialize height station",PRM);
  }



  /* Allocation output */
  VerifFunction( InitializeOutputs(PRM.statout, &OUT, PRM ), " MapSeismograms", PRM );

#if ( VERBOSE > 0 )
  VerifFunction( PrintInfoMedium( ANL, MDM, SRC, PRM), "InfoMedium", PRM);
#endif

  /* ============================ */
  /* Beginning of the iterations */
  /* ============================ */
  /*** Prepare iterations ***/
  VerifFunction( EXIT_SUCCESS, "Beginning of the iteration",PRM);


#ifdef MISS
  if ( retval=PAPI_reset(EventSet) != PAPI_OK ){ printf("ERROR stop\n");}
  if ( retval=PAPI_start(EventSet) != PAPI_OK ){ printf("ERROR start\n");}
#endif

#ifdef FLOPS
  if ( (retval=PAPI_flops(&ireal_time,&iproc_time,&iflpops,&imflops)) < PAPI_OK ){
    printf("Could not initialize PAPI_flops\n");
    printf("Your platform may not support floating point operation event.\n");
    printf("retval: %d\n", retval);
    exit(EXIT_FAILURE);
  }
#endif

#if (TIMER==1)
  timing3 = my_second();
#endif

#if (TIMER==2)
#ifdef MPI
  MPI_Barrier (MPI_COMM_WORLD);
#endif
  timing3 = my_second();
#endif


#ifdef MPI
#if (COMM==1) /* Prepare Sending & Receiving  messages */
  MPI_Barrier (MPI_COMM_WORLD);
  /* == Stress == */
  /* East - West */
  /*  E->W */
  if ( EAST.rank != -1 )
    MPI_Send_init( EAST.bufT0S, 6*EAST.nmax, MPI_DOUBLE,
                   EAST.rank, EAST.channelT0S, MPI_COMM_WORLD, &reqT0S[3]);

  if ( WEST.rank != -1 )
    MPI_Recv_init(WEST.bufT0R, 6*WEST.nmax, MPI_DOUBLE,
				  WEST.rank, WEST.channelT0R, MPI_COMM_WORLD, &reqT0R[3]);

  /* W->E */
  if ( WEST.rank != -1 )
    MPI_Send_init(WEST.bufT0S, 6*WEST.nmax, MPI_DOUBLE,
				  WEST.rank, WEST.channelT0S, MPI_COMM_WORLD, &reqT0S[4]);

  if ( EAST.rank != -1 )
    MPI_Recv_init(EAST.bufT0R, 6*EAST.nmax, MPI_DOUBLE,
				  EAST.rank, EAST.channelT0R, MPI_COMM_WORLD, &reqT0R[4]);

  /* North - South */
  /* N->S */
  if ( NORTH.rank != -1 )
    MPI_Send_init(NORTH.bufT0S, 6*NORTH.nmax, MPI_DOUBLE,
				  NORTH.rank, NORTH.channelT0S, MPI_COMM_WORLD, &reqT0S[1]);

  if ( SOUTH.rank != -1 )
	MPI_Recv_init(SOUTH.bufT0R, 6*SOUTH.nmax, MPI_DOUBLE,
				  SOUTH.rank, SOUTH.channelT0R, MPI_COMM_WORLD, &reqT0R[1]);
  /* S->N */
  if ( SOUTH.rank != -1 )
    MPI_Send_init(SOUTH.bufT0S, 6*SOUTH.nmax, MPI_DOUBLE,
				  SOUTH.rank, SOUTH.channelT0S, MPI_COMM_WORLD, &reqT0S[2]);

  if ( NORTH.rank != -1 )
    MPI_Recv_init(NORTH.bufT0R, 6*NORTH.nmax, MPI_DOUBLE,
				  NORTH.rank, NORTH.channelT0R, MPI_COMM_WORLD, &reqT0R[2]);

  /* == Velocity == */
  /* East - West */
  /* E->W */
  if ( EAST.rank != -1 )
    MPI_Send_init(EAST.bufV0S, 3*EAST.nmax, MPI_DOUBLE,
				  EAST.rank, EAST.channelV0S, MPI_COMM_WORLD, &reqV0S[3]);

  if ( WEST.rank != -1 )
    MPI_Recv_init(WEST.bufV0R, 3*WEST.nmax, MPI_DOUBLE,
				  WEST.rank, WEST.channelV0R, MPI_COMM_WORLD, &reqV0R[3]);

  /* W->E */
  if ( WEST.rank != -1 )
    MPI_Send_init(WEST.bufV0S, 3*WEST.nmax, MPI_DOUBLE,
				  WEST.rank, WEST.channelV0S, MPI_COMM_WORLD, &reqV0S[4]);

  if ( EAST.rank != -1 )
    MPI_Recv_init(EAST.bufV0R, 3*EAST.nmax, MPI_DOUBLE,
				  EAST.rank, EAST.channelV0R, MPI_COMM_WORLD, &reqV0R[4]);

  /* North - South */
  /* N->S */
  if ( NORTH.rank != -1 )
    MPI_Send_init(NORTH.bufV0S, 3*NORTH.nmax, MPI_DOUBLE,
				  NORTH.rank, NORTH.channelV0S, MPI_COMM_WORLD, &reqV0S[1]);

  if ( SOUTH.rank != -1 )
    MPI_Recv_init(SOUTH.bufV0R, 3*SOUTH.nmax, MPI_DOUBLE,
				  SOUTH.rank, SOUTH.channelV0R, MPI_COMM_WORLD, &reqV0R[1]);

  /* S->N */
  if ( SOUTH.rank != -1 )
    MPI_Send_init(SOUTH.bufV0S, 3*SOUTH.nmax, MPI_DOUBLE,
				  SOUTH.rank, SOUTH.channelV0S, MPI_COMM_WORLD, &reqV0S[2]);

  if ( NORTH.rank != -1 )
    MPI_Recv_init(NORTH.bufV0R, 3*NORTH.nmax, MPI_DOUBLE,
				  NORTH.rank, NORTH.channelV0R, MPI_COMM_WORLD, &reqV0R[2]);


  /* == Ksil == */
  if ( ANLmethod == KRISTEKandMOCZO ){
	/* East - West */
	/* E->W */
	if ( EAST.rank != -1 )
	  MPI_Send_init(EAST.bufKsiS, 6*EAST.nmax, MPI_DOUBLE,
					EAST.rank, EAST.channelKsiS, MPI_COMM_WORLD, &reqKsiS[3]);

	if ( WEST.rank != -1 )
	  MPI_Recv_init(WEST.bufKsiR, 6*WEST.nmax, MPI_DOUBLE,
					WEST.rank, WEST.channelKsiR, MPI_COMM_WORLD, &reqKsiR[3]);

	/* W->E */
	if ( WEST.rank != -1 )
	  MPI_Send_init(WEST.bufKsiS, 6*WEST.nmax, MPI_DOUBLE,
					WEST.rank, WEST.channelKsiS, MPI_COMM_WORLD, &reqKsiS[4]);

	if ( EAST.rank != -1 )
	  MPI_Recv_init(EAST.bufKsiR, 6*EAST.nmax, MPI_DOUBLE,
					EAST.rank, EAST.channelKsiR, MPI_COMM_WORLD, &reqKsiR[4]);

	/* North - South */
	/* N->S */
	if ( NORTH.rank != -1 )
	  MPI_Send_init(NORTH.bufKsiS, 6*NORTH.nmax, MPI_DOUBLE,
					NORTH.rank, NORTH.channelKsiS, MPI_COMM_WORLD, &reqKsiS[1]);

	if ( SOUTH.rank != -1 )
	  MPI_Recv_init(SOUTH.bufKsiR, 6*SOUTH.nmax, MPI_DOUBLE,
					SOUTH.rank, SOUTH.channelKsiR, MPI_COMM_WORLD, &reqKsiR[1]);

	/* S->N */
	if ( SOUTH.rank != -1 )
	  MPI_Send_init(SOUTH.bufKsiS, 6*SOUTH.nmax, MPI_DOUBLE,
					SOUTH.rank, SOUTH.channelKsiS, MPI_COMM_WORLD, &reqKsiS[2]);

	if ( NORTH.rank != -1 )
	  MPI_Recv_init(NORTH.bufKsiR, 6*NORTH.nmax, MPI_DOUBLE,
					NORTH.rank, NORTH.channelKsiR, MPI_COMM_WORLD, &reqKsiR[2]);

  } /* end ksil comm */
  MPI_Barrier (MPI_COMM_WORLD);

#endif  /* PERSISTANT */

#endif /* MPI */
  /* useful mapping of PRM*/
  XMIN= PRM.xMin;
  XMAX= PRM.xMax;
  YMIN= PRM.yMin;
  YMAX= PRM.yMax;
  ZMIN= PRM.zMin;
  ZMAX= PRM.zMax;
  ZMAX0= PRM.zMax0;
  DELTA= PRM.delta;
  DT= PRM.dt;
  DS= PRM.ds;
  MPMX= PRM.mpmx;
  MPMY= PRM.mpmy;
  TMAX= PRM.tMax;

  /*** iterate on time step ***/
  for ( l = 1; l <= PRM.tMax; l++ ){
	time = DT * l;
#if(VERBOSE > 0)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	if (PRM.me == 0 ) {fprintf(stderr,"=== time step : %i === \n",l);}
#endif
	if ( source == HISTFILE )
      		fStatus=computeSeisMoment(&SRC, time, PRM ) ;

#if(VERBOSE > 1)
    	VerifFunction(fStatus , "increment seismic moment"  ,PRM);
#endif

#if (TIMER==2)
#ifdef MPI
      	MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing1 = my_second();
#endif

#if (TIMER==1)
	timing1 = my_second();
#endif

	/* Calculation */
	/* === First step : t = l + 1/2 for stress ===*/

#ifdef PROFILE2
	TAU_PROFILE_START (compute_sig);
#endif

	/* computation of intermediates :
	   Phiv (CPML), t??? (PML), ksi (Day & Bradley), ksil (Kristek and Moczo) */
	/* imode : to increase the velocity of the computation, we begin by computing
	   the values of ksil at the boundaries of the px * py parts of the array
	   Afterwise, we can compute the values of ksil in the middle */
#if (VERBOSE > 1)
#ifdef MPI
      	MPI_Barrier (MPI_COMM_WORLD);
#endif
	if( PRM.me == 0){	fprintf(stderr,"/* Compute Intermediates : l */ \n ", l);}
#endif

	for ( imode = 1; imode <= 5; imode++){

	  if ( imode == 1 ){
		mpmx_begin = 1;
		mpmx_end = 3;
		mpmy_begin = 1;
		mpmy_end = MPMY;
	  }

	  if ( imode == 2 ){
		mpmx_begin = MPMX-2;
		mpmx_end = MPMX;
		mpmy_begin = 1;
		mpmy_end = MPMY;
	  }

	  if ( imode == 3 ){
		mpmy_begin = 1;
		mpmy_end = 3;
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
	  }

	  if ( imode == 4 ){
		mpmy_begin = MPMY-2;
		mpmy_end = MPMY;
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
	  }

	  if ( imode == 5 ){ /* imode = 5 --> middle of each part of the array */
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
		mpmy_begin = 4;
		mpmy_end = MPMY-3;
		/* COMMUNICATIONS */
#ifdef MPI
#if (COMM==1)
		if ( ANLmethod == KRISTEKandMOCZO ){
		  /* We are sending computed Ksil for computeStress */
		  /*  0: send, 1: receive */
		  SyncBufKsil( &ANL, 0, &NORTH, PRM);
		  SyncBufKsil( &ANL, 0, &SOUTH, PRM);
		  SyncBufKsil( &ANL, 0, &EAST, PRM);
		  SyncBufKsil( &ANL, 0, &WEST, PRM);
		  /* start send */
		  if ( NORTH.rank != -1 ) MPI_Start(&reqKsiS[1]);
		  if ( SOUTH.rank != -1 ) MPI_Start(&reqKsiS[2]);
		  if ( EAST.rank != -1 ) MPI_Start(&reqKsiS[3]);
		  if ( WEST.rank != -1 ) MPI_Start(&reqKsiS[4]);
		  /* start receive */
		  if ( SOUTH.rank != -1 ) MPI_Start(&reqKsiR[1]);
		  if ( NORTH.rank != -1 ) MPI_Start(&reqKsiR[2]);
		  if ( WEST.rank != -1 ) MPI_Start(&reqKsiR[3]);
		  if ( EAST.rank != -1 ) MPI_Start(&reqKsiR[4]);

		} /* end ANLmethod == KRISTEKandMOCZO */
#endif /* end of first method */
#endif

	  }  /* end of imode = 5 */
#ifndef NOINTERMEDIATES
	  ComputeIntermediates(	&ABC,&ANL,
							/* Parameters */
							v0, PRM, MDM,
							mpmx_begin, mpmx_end, /* local computed domain */
							mpmy_begin, mpmy_end
							);
#endif
	} /* end of for imode from 1 to 5 */
      /* end of computation of intermediates */

      /* COMMUNICATE KSIL (K&M anelasticity method) */
	if ( ANLmethod == KRISTEKandMOCZO ){
#ifdef MPI
#if (COMM==1)
	  /* We are receiving computed Ksil for computeStress */
	  /* wait until that is really sent */
	  if ( NORTH.rank != -1 ) MPI_Wait(&reqKsiS[1],&statKsiS[1]);
	  if ( SOUTH.rank != -1 ) MPI_Wait(&reqKsiS[2],&statKsiS[2]);
	  if ( EAST.rank != -1 )  MPI_Wait(&reqKsiS[3],&statKsiS[3]);
	  if ( WEST.rank != -1 )  MPI_Wait(&reqKsiS[4],&statKsiS[4]);

	  /* wait until that is really received */
	  if ( SOUTH.rank != -1 ) MPI_Wait(&reqKsiR[1],&statKsiR[1]);
	  if ( NORTH.rank != -1 ) MPI_Wait(&reqKsiR[2],&statKsiR[2]);
	  if ( WEST.rank != -1 ) MPI_Wait(&reqKsiR[3],&statKsiR[3]);
	  if ( EAST.rank != -1 ) MPI_Wait(&reqKsiR[4],&statKsiR[4]);

	  /*  0: send, 1: receive */
	  SyncBufKsil( &ANL, 1, &NORTH, PRM);
	  SyncBufKsil( &ANL, 1, &SOUTH, PRM);
	  SyncBufKsil( &ANL, 1, &EAST, PRM);
	  SyncBufKsil( &ANL, 1, &WEST, PRM);
#endif /* end of first method */
#if (COMM==2)
	  /* Synchronize Sending buffers */
      	  /*  0: send, 1: receive */
      	  SyncBufKsil( &ANL, 0, &WEST, PRM);
      	  SyncBufKsil( &ANL, 0, &EAST, PRM);
      	  SyncBufKsil( &ANL, 0, &NORTH, PRM);
      	  SyncBufKsil( &ANL, 0, &SOUTH, PRM);

          /* Communicate  */
    	  /* E->W */
      	  if ( EAST.rank != -1 ){ MPI_Isend(EAST.bufKsiS, 6*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelKsiS, MPI_COMM_WORLD, &req[3]); }
      	  if ( WEST.rank != -1 ){ MPI_Recv(WEST.bufKsiR, 6*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelKsiR, MPI_COMM_WORLD, &status[3]);}
      	  else{ MPI_Wait(&req[3], &status[3]); }


      	  /* W->E */
      	  if ( WEST.rank != -1 ){MPI_Isend(WEST.bufKsiS, 6*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelKsiS, MPI_COMM_WORLD, &req[4]);}
      	  if ( EAST.rank != -1 ){MPI_Recv( EAST.bufKsiR, 6*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelKsiR, MPI_COMM_WORLD, &status[4]);}
      	  else{ MPI_Wait(&req[4], &status[4]); }

      /* N->S */
      if ( NORTH.rank != -1 ){MPI_Isend(NORTH.bufKsiS, 6*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelKsiS, MPI_COMM_WORLD, &req[1]);}
      if ( SOUTH.rank != -1 ){MPI_Recv(SOUTH.bufKsiR, 6*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelKsiR, MPI_COMM_WORLD, &status[1]);}
      else{ MPI_Wait(&req[1], &status[1]); }

      /* S->N */
      if ( SOUTH.rank != -1 ){MPI_Isend(SOUTH.bufKsiS, 6*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelKsiS, MPI_COMM_WORLD, &req[2]);}
      if ( NORTH.rank != -1){MPI_Recv(NORTH.bufKsiR, 6*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelKsiR, MPI_COMM_WORLD, &status[2]);}
      else{ MPI_Wait(&req[2], &status[2]); }

      /* Synchronize Received buffers */
      /*  0: send, 1: receive */
      SyncBufKsil( &ANL, 1, &EAST, PRM);
      SyncBufKsil( &ANL, 1, &WEST, PRM);
      SyncBufKsil( &ANL, 1, &SOUTH, PRM);
      SyncBufKsil( &ANL, 1, &NORTH, PRM);

#endif	/* end of BLOCKING communication method */
#endif /* MPI */
	} /* end ANLmethod == KRISTEKandMOCZO */

      /* imode : to increase the velocity of the computation, we begin by computing
		 the values of stress at the boundaries of the px * py parts of the array
		 Afterwise, we can compute the values of the stress in the middle */
	for ( imode = 1; imode <= 5;imode++){

	  if ( imode == 1 ){
		mpmx_begin = 1;
		mpmx_end = 3;
		mpmy_begin = 1;
		mpmy_end = MPMY;
	  }

	  if ( imode == 2 ){
		mpmx_begin = MPMX-2;
		mpmx_end = MPMX;
		mpmy_begin = 1;
		mpmy_end = MPMY;
	  }

	  if ( imode == 3 ){
		mpmy_begin = 1;
		mpmy_end = 3;
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
	  }

	  if ( imode == 4 ){
		mpmy_begin = MPMY-2;
		mpmy_end = MPMY;
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
	  }

	  if ( imode == 5 ){ /* imode = 5 --> middle of each part of the array */
		mpmx_begin = 4;
		mpmx_end = MPMX-3;
		mpmy_begin = 4;
		mpmy_end = MPMY-3;

		/* Communications
		 *   first method & second method
		 * what only differs communications
		 */
#ifdef MPI
#if (COMM==1)

		/* We are sending computed stress for computeVelocity */
		/*  0: send, 1: receive */
		SyncBufStress( &t0, 0, &NORTH, PRM);
		SyncBufStress( &t0, 0, &SOUTH, PRM);
		SyncBufStress( &t0, 0, &EAST, PRM);
		SyncBufStress( &t0, 0, &WEST, PRM);

		/* start send */
		if ( NORTH.rank != -1 ) MPI_Start(&reqT0S[1]);
		if ( SOUTH.rank != -1 ) MPI_Start(&reqT0S[2]);
		if ( EAST.rank != -1 ) MPI_Start(&reqT0S[3]);
		if ( WEST.rank != -1 ) MPI_Start(&reqT0S[4]);


		/* start receive */
		if ( SOUTH.rank != -1 ) MPI_Start(&reqT0R[1]);
		if ( NORTH.rank != -1 ) MPI_Start(&reqT0R[2]);
		if ( WEST.rank != -1 ) MPI_Start(&reqT0R[3]);
		if ( EAST.rank != -1 ) MPI_Start(&reqT0R[4]);
#endif /* end of first method */
#endif /* MPI */	 

	 } /* end of imode = 5 */
	  /* Beginning of stress computation */
#if(VERBOSE > 1)
//	  MPI_Barrier (MPI_COMM_WORLD);
	  if( PRM.me == 0){ fprintf(stderr," /* Beginning of stress computation : %li */ \n", l );}
#endif

#ifndef NOSTRESS
	  ComputeStress(&t0,
					/* INPUTS */
					v0, MDM, PRM, ABC, ANL,
					mpmx_begin,mpmx_end, /* local computed domain */
					mpmy_begin,mpmy_end
					);
#endif

	} /* end of imode */


#if (VERBOSE > 1)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
	if ( PRM.me == 0 ){ perror("## End of Stress ");  }
#endif /* MPI*/
#endif

#ifdef PROFILE2
	TAU_PROFILE_STOP (compute_sig);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing2 = my_second();
	timing_bc1 = timing_bc1 + (timing2-timing1);
#endif

#if (TIMER==1)
	timing2 = my_second();
	timing_bc1 = timing_bc1 + (timing2-timing1);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing1 = my_second();
#endif

#if (TIMER==1)
	timing1 = my_second();
#endif

#ifdef PROFILE1
	TAU_PROFILE_START (exchange_sig);
#endif

#ifdef PROFILE2
	TAU_PROFILE_START (exchange_sig);
#endif


	/* Communication : first method */
#ifdef MPI
#if (COMM==1)

	/* wait until that is really sent */
	if ( NORTH.rank != -1 ) MPI_Wait(&reqT0S[1],&statT0S[1]);
	if ( SOUTH.rank != -1 ) MPI_Wait(&reqT0S[2],&statT0S[2]);
	if ( EAST.rank != -1 )  MPI_Wait(&reqT0S[3],&statT0S[3]);
	if ( WEST.rank != -1 )  MPI_Wait(&reqT0S[4],&statT0S[4]);

	/* wait until that is really received */
	if ( SOUTH.rank != -1 ) MPI_Wait(&reqT0R[1],&statT0R[1]);
	if ( NORTH.rank != -1 ) MPI_Wait(&reqT0R[2],&statT0R[2]);
 	if ( WEST.rank != -1 ) MPI_Wait(&reqT0R[3],&statT0R[3]);
 	if ( EAST.rank != -1 ) MPI_Wait(&reqT0R[4],&statT0R[4]);

	/*  0: send, 1: receive */
	SyncBufStress( &t0, 1, &NORTH, PRM);
	SyncBufStress( &t0, 1, &SOUTH, PRM);
	SyncBufStress( &t0, 1, &EAST, PRM);
	SyncBufStress( &t0, 1, &WEST, PRM);

#endif /* end of first method */

#if (COMM==2)
	/* Communication to synchronize */

	/* Synchronize Sending buffers */
	/*  0: send, 1: receive */
	SyncBufStress( &t0, 0, &WEST, PRM);
	SyncBufStress( &t0, 0, &EAST, PRM);
	SyncBufStress( &t0, 0, &NORTH, PRM);
	SyncBufStress( &t0, 0, &SOUTH, PRM);

	/* Communicate  */
	/* E->W */
	if ( EAST.rank != -1 ){ MPI_Isend(EAST.bufT0S, 6*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelT0S, MPI_COMM_WORLD, &req[3]); }
	if ( WEST.rank != -1 ){ MPI_Recv(WEST.bufT0R, 6*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelT0R, MPI_COMM_WORLD, &status[3]);}
	else{ MPI_Wait(&req[3], &status[3]); }

	/* W->E */
	if ( WEST.rank != -1 ){MPI_Isend(WEST.bufT0S, 6*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelT0S, MPI_COMM_WORLD, &req[4]);}
	if ( EAST.rank != -1 ){MPI_Recv( EAST.bufT0R, 6*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelT0R, MPI_COMM_WORLD, &status[4]);}
	else{ MPI_Wait(&req[4], &status[4]); }

	/* N->S */
	if ( NORTH.rank != -1 ){MPI_Isend(NORTH.bufT0S, 6*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelT0S, MPI_COMM_WORLD, &req[1]);}
	if ( SOUTH.rank != -1 ){MPI_Recv(SOUTH.bufT0R, 6*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelT0R, MPI_COMM_WORLD, &status[1]);}
	else{ MPI_Wait(&req[1], &status[1]); }

	/* S->N */
	if ( SOUTH.rank != -1 ){MPI_Isend(SOUTH.bufT0S, 6*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelT0S, MPI_COMM_WORLD, &req[2]);}
	if ( NORTH.rank != -1){MPI_Recv(NORTH.bufT0R, 6*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelT0R, MPI_COMM_WORLD, &status[2]);}
	else{ MPI_Wait(&req[2], &status[2]); }

	/* Synchronize Received buffers */
	/*  0: send, 1: receive */
	SyncBufStress( &t0, 1, &EAST, PRM);
	SyncBufStress( &t0, 1, &WEST, PRM);
	SyncBufStress( &t0, 1, &SOUTH, PRM);
	SyncBufStress( &t0, 1, &NORTH, PRM);

#endif /* end of third method */
#endif /* MPI*/


#ifdef PROFILE1
	TAU_PROFILE_STOP (exchange_sig);
#endif

#ifdef PROFILE2
	TAU_PROFILE_STOP (exchange_sig);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif /* MPI */
	timing2 = my_second();
	timing_comm1 = timing_comm1 + (timing2-timing1);
#endif

#if (TIMER==1)
	timing2 = my_second();
	timing_comm1 = timing_comm1 + (timing2-timing1);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif /* MPI */	
	timing1 = my_second();
#endif

#if (TIMER==1)
	timing1 = my_second();
#endif


	/* imode : to increase the velocity of the computation, we begin by computing
	   the values of stress at the boundaries of the px * py parts of the array
	   Afterwise, we can compute the values of the stress in the middle */
	for ( imode = 1; imode <= 5; imode++){

	  if ( imode == 1 ){
		mpmx_begin = 1;
		mpmx_end = 3;
		mpmy_begin = 1;
		mpmy_end = MPMY;
	  }

	  if ( imode == 2 ){
	    mpmx_begin = MPMX-2;
	    mpmx_end = MPMX;
	    mpmy_begin = 1;
	    mpmy_end = MPMY;
	  }

	  if ( imode == 3 ){
	    mpmy_begin = 1;
	    mpmy_end = 3;
	    mpmx_begin = 4;
	    mpmx_end = MPMX-3;
	  }

      if ( imode == 4 ){
	    mpmy_begin = MPMY-2;
	    mpmy_end = MPMY;
	    mpmx_begin = 4;
	    mpmx_end = MPMX-3;
	  }

      if ( imode == 5 ){ /* imode = 5 --> middle of each part of the array */
	    mpmx_begin = 4;
	    mpmx_end = MPMX-3;
	    mpmy_begin = 4;
	    mpmy_end = MPMY-3;

		/* Communication : first method */

#ifdef MPI
#if (COMM==1)
	    /* We are sending computed velocity */
	    /*  0: send, 1: receive */
	    SyncBufVelocity( &v0, 0, &NORTH, PRM);
	    SyncBufVelocity( &v0, 0, &SOUTH, PRM);
	    SyncBufVelocity( &v0, 0, &EAST, PRM);
	    SyncBufVelocity( &v0, 0, &WEST, PRM);

	    if ( NORTH.rank != -1 ) MPI_Start(&reqV0S[1]);
	    if ( SOUTH.rank != -1 ) MPI_Start(&reqV0S[2]);
	    if ( EAST.rank != -1 ) MPI_Start(&reqV0S[3]);
	    if ( WEST.rank != -1 ) MPI_Start(&reqV0S[4]);

	    if ( SOUTH.rank != -1 ) MPI_Start(&reqV0R[1]);
	    if ( NORTH.rank != -1 ) MPI_Start(&reqV0R[2]);
	    if ( WEST.rank != -1 ) MPI_Start(&reqV0R[3]);
	    if ( EAST.rank != -1 ) MPI_Start(&reqV0R[4]);
#endif /* end of first method */
#endif /* MPI */

      } /* end of imode = 5 */

	  /* Beginning of velocity computation */
#if(VERBOSE > 1)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif

      	if( PRM.me == 0){	fprintf(stderr," ## begin velocity computation : %i */ \n", l); }
#endif
#ifndef NOVELOCITY
      computeVelocity(&v0, &ABC,
                      /* INPUTS */
                      t0, MDM, PRM, ANL, SRC,
                      mpmx_begin, mpmx_end, /* local computed domain */
                      mpmy_begin, mpmy_end,
                      l 	/* time index */
                      );

#endif

    } /* end of imode */

    if ( source == VELO ){

      computeSource (&v0,
                     PRM,
                     MDM,
                     SRC,
                     l);

    }

#if(VERBOSE > 1)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif

	if( PRM.me == 0){	perror(" ## end velocity */ \n"); }
#endif

#ifdef PROFILE2
	TAU_PROFILE_STOP (compute_vit);
#endif

#if (TIMER==2)
#ifdef MPI
		    MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing2 = my_second();
	timing_bc2 = timing_bc2 + (timing2-timing1);
#endif

#if (TIMER==1)
	timing2 = my_second();
	timing_bc2 = timing_bc2 + (timing2-timing1);
#endif

#if (TIMER==2)
#ifdef MPI
		    MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing1 = my_second();
#endif

#if (TIMER==1)
	timing1 = my_second();
#endif

#ifdef PROFILE1
	TAU_PROFILE_START (exchange_vit);
#endif

#ifdef PROFILE2
	TAU_PROFILE_START (exchange_vit);
#endif

#if(VERBOSE > 1)
#ifdef MPI
		MPI_Barrier (MPI_COMM_WORLD);
		if( PRM.me == 0){	perror("\n /* Beginning velocity communications */ \n");}
#endif
#endif

#ifdef MPI
#if (COMM==1)       /* Communication : first method */
	/* wait until that is really sent */
	if ( NORTH.rank != -1 ) MPI_Wait(&reqV0S[1],&statV0S[1]);
	if ( SOUTH.rank != -1 ) MPI_Wait(&reqV0S[2],&statV0S[2]);
	if ( EAST.rank != -1 )  MPI_Wait(&reqV0S[3],&statV0S[3]);
	if ( WEST.rank != -1 )  MPI_Wait(&reqV0S[4],&statV0S[4]);

	/* wait until that is really received */
	if ( SOUTH.rank != -1  ) MPI_Wait(&reqV0R[1],&statV0R[1]);
	if ( NORTH.rank != -1  ) MPI_Wait(&reqV0R[2],&statV0R[2]);
	if ( WEST.rank != -1 )  MPI_Wait(&reqV0R[3],&statV0R[3]);
	if ( EAST.rank != -1 )  MPI_Wait(&reqV0R[4],&statV0R[4]);

	/* synchronise Receiving buffers */
	/*  0: send, 1: receive */
	SyncBufVelocity( &v0, 1, &NORTH, PRM);
	SyncBufVelocity( &v0, 1, &SOUTH, PRM);
	SyncBufVelocity( &v0, 1, &EAST, PRM);
	SyncBufVelocity( &v0, 1, &WEST, PRM);
#endif /* end of first method */


#if(BLOCKING)  /* Communication : third method */
	/* Synchronize Sending buffers */
	/*  0: send, 1: receive */

	SyncBufVelocity( &v0, 0, &WEST, PRM);
	SyncBufVelocity( &v0, 0, &EAST, PRM);
	SyncBufVelocity( &v0, 0, &NORTH, PRM);
	SyncBufVelocity( &v0, 0, &SOUTH, PRM);

	/* Communicate  */
	/* E->W */
	if ( EAST.rank != -1 ){ MPI_Isend(EAST.bufV0S, 3*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelV0S, MPI_COMM_WORLD, &req[3]); }
	if ( WEST.rank != -1 ){ MPI_Recv(WEST.bufV0R, 3*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelV0R, MPI_COMM_WORLD, &status[3]);}
	else{ MPI_Wait(&req[3], &status[3]); }

	/* W->E */
	if ( WEST.rank != -1 ){MPI_Isend(WEST.bufV0S, 3*WEST.nmax, MPI_DOUBLE, WEST.rank, WEST.channelV0S, MPI_COMM_WORLD, &req[4]);}
	if ( EAST.rank != -1 ){ MPI_Recv( EAST.bufV0R, 3*EAST.nmax, MPI_DOUBLE, EAST.rank, EAST.channelV0R, MPI_COMM_WORLD, &status[4]);}
	else{ MPI_Wait(&req[4], &status[4]); }

	/* N->S */
	if ( NORTH.rank != -1 ){MPI_Isend(NORTH.bufV0S, 3*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelV0S, MPI_COMM_WORLD, &req[1]);}
	if ( SOUTH.rank != -1 ){MPI_Recv(SOUTH.bufV0R, 3*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelV0R, MPI_COMM_WORLD, &status[1]);}
	else{ MPI_Wait(&req[1], &status[1]); }

	/* S->N */
	if ( SOUTH.rank != -1 ){MPI_Isend(SOUTH.bufV0S, 3*SOUTH.nmax, MPI_DOUBLE, SOUTH.rank, SOUTH.channelV0S, MPI_COMM_WORLD, &req[2]);}
	if (  NORTH.rank != -1){MPI_Recv(NORTH.bufV0R, 3*NORTH.nmax, MPI_DOUBLE, NORTH.rank, NORTH.channelV0R, MPI_COMM_WORLD, &status[2]);}
	else{ MPI_Wait(&req[2], &status[2]); }

	/* Synchronize Received buffers */
	/*  0: send, 1: receive */
	SyncBufVelocity( &v0, 1, &EAST, PRM);
	SyncBufVelocity( &v0, 1, &WEST, PRM);
	SyncBufVelocity( &v0, 1, &SOUTH, PRM);
	SyncBufVelocity( &v0, 1, &NORTH, PRM);

#endif /* end of third method */
#endif /* MPI */

#ifdef PROFILE1
	TAU_PROFILE_STOP (exchange_vit);
#endif

#ifdef PROFILE2
	TAU_PROFILE_STOP (exchange_vit);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing2 = my_second();
	timing_comm2 = timing_comm2 + (timing2-timing1);
#endif

#if (TIMER==1)
	timing2 = my_second();
	timing_comm2 = timing_comm2 + (timing2-timing1);
#endif

#if (TIMER==2)
#ifdef MPI
	MPI_Barrier (MPI_COMM_WORLD);
#endif
	timing1 = my_second();
	if(PRM.me == 0)
		timingStart = my_second();
#endif

#if (TIMER==1)
	timing1 = my_second();
#endif

	/* === generate outputs ===*/

    	/* seismograms (station files) */
	/* === Calculation of the seismograms === */
	fStatus=ComputeSeismograms( &OUT, v0, t0, PRM, l-1);

	if ( (l%PRM.statout == 0) ||  l == TMAX ){
#if (VERBOSE > 1)
      VerifFunction(EXIT_SUCCESS,"begin communication of seismogramms",PRM);
#endif

#if (TIMER==2)
	MPI_Barrier (MPI_COMM_WORLD);
	if(PRM.me == 0)
		time_before_output = my_second();
#endif

	  if(!strcmp("parallel", PRM.optStation)){
	  	int pr = 0;
	        for(pr = 0; pr < np; pr++)
        	    files_per_process[pr]=0;
      	  }

	  for ( ir = 0; ir < OUT.iObs; ir++){ //for all station files we could write
		if ( OUT.ista[ir] == 1 ){ //only write if it is inside the domain
		    if(!strcmp("parallel", PRM.optStation)){
			write_station_files_parallel_communicationstep(&PRM, &OUT, writer_rank, files_per_process, ir, comm2d);
		    } /* end if parallel station output */
		    else if(!strcmp("none", PRM.optStation))
		    {
			write_station_files_nooptimization(&PRM, &OUT, ir, comm2d, l, &write_buffer, local_buf);
		    } /* end if station output without optimizations */
		    else if(!strcmp("MPIIO", PRM.optStation))
		    {
			write_station_files_mpiio(&PRM, &OUT, ir, comm2d, local_buf, mpiio_station_buffer, l);
		    } /* end if station output with MPI-IO */
		} /* end if station is inside domain */
	  } /* end for each station file */

	//parallel station need to write files (only now outside of the first loop because we want to do all communications before)
  	  if(!strcmp("parallel", PRM.optStation)){
  		//second part: writer ranks will write their files
  		for ( ir = 0; ir < OUT.iObs; ir++){ //we'll write up to OUT.iObs files 
	  		if ( OUT.ista[ir] == 1 ){
  				//now the writer rank will create the file
  				if ( PRM.me == writer_rank[ir] ){
					write_station_files_parallel_IOstep(&PRM, &OUT, ir, l, local_buf, &write_buffer);
				} /* end of PRM.me = writer_rank */
			} /* end of station is inside domain */
    		} /* end for each station */
  	  }

    	  /* blank seis_output for next values */
	  for ( i = 0; i < PRM.statout;i++ ){
      		for ( ir = 0; ir < OUT.iObs; ir++){
          		for ( j = 1; j <= 9 ; j++){ /* each field */
            			OUT.seis_output[i][ir][j]=0.;
          		}
        	}
      	  }
#if (TIMER==2)
	MPI_Barrier (MPI_COMM_WORLD);
	if(PRM.me == 0)
		timing_IO_station += my_second() - time_before_output;
#endif
    } /* end output seismograms at l%STATION_STEP */

#if (VERBOSE > 1)
	VerifFunction(fStatus,"compute Seismograms",PRM);
#endif

    /* === Calculation of the snapshots === */
    if ( snapType == ODISPL || snapType == OBOTH){
      /* xy surface */
      int K0;
      const double DT=PRM.dt;
      if ( surface == ABSORBING ){ K0=OUT.k0;}

      for ( i = -1; i<= MPMX+2; i++){
        for ( j = -1; j<= MPMY+2; j++){
          if ( surface == ABSORBING ){
            OUT.Uxy[1][i][j]+=DT*v0.x[i][j][OUT.k0];
            OUT.Uxy[2][i][j]+=DT*v0.y[i][j][OUT.k0];
            OUT.Uxy[3][i][j]+=DT*v0.z[i][j][OUT.k0];
          } else if ( surface == FREE ){
            OUT.Uxy[1][i][j]+=DT*v0.x[i][j][1];
            OUT.Uxy[2][i][j]+=DT*v0.y[i][j][1];
            OUT.Uxy[3][i][j]+=DT*v0.z[i][j][0];
          }
        }
      }
      /* xz surface */
      jcpu = PRM.j2jcpu_array[OUT.j0];
      jmp_tmp =  PRM.j2jmp_array[OUT.j0];
      if ( PRM.coords[1] == jcpu ){
        for ( i = -1; i<= MPMX+2; i++){
          for ( k = ZMIN-DELTA; k<= ZMAX0; k++){
            OUT.Uxz[1][i][k]+=DT*v0.x[i][jmp_tmp][k];
            OUT.Uxz[2][i][k]+=DT*v0.y[i][jmp_tmp][k];
            OUT.Uxz[3][i][k]+=DT*v0.z[i][jmp_tmp][k];
          }
        }
      }
      /* yz surface */
      icpu = PRM.i2icpu_array[OUT.j0];
      imp_tmp =  PRM.i2imp_array[OUT.j0];
      if ( PRM.coords[0] == icpu ){
        for ( j = -1; j<= MPMY+2; j++){
          for ( k = ZMIN-DELTA; k<= ZMAX0; k++){
            OUT.Uyz[1][j][k]+=DT*v0.x[imp_tmp][j][k];
            OUT.Uyz[2][j][k]+=DT*v0.y[imp_tmp][j][k];
            OUT.Uyz[3][j][k]+=DT*v0.z[imp_tmp][j][k];
          }
      }
      }
      VerifFunction(EXIT_SUCCESS,"compute displacement snapshots",PRM);
    } /* end calculation */

    /*== write snapshots (surface files) ==*/
    if (( (l % PRM.surfout) == 0 ) || ( l == PRM.tMax)) {
	
#if (TIMER==2)
	MPI_Barrier (MPI_COMM_WORLD);
	if(PRM.me == 0)
		time_before_output = my_second();
#endif
        int index;

        //I'll need to know my coodinates
        y = PRM.me/PRM.px; //TODO maybe these values were already available somewhere, no need to recalculate y = PRM.coords[1] e x = PRM.coords[0]
        x = PRM.me - y*PRM.px;

	int snapStep, snapStepMax=1;
	if ( snapType == OBOTH ) {snapStepMax = 2;}

        if(!strcmp("MPIIO", PRM.optSurface))
	{
		//allocate buffer to be used in the output of the three files (to make it easier and avoid allocating and deallocating buffers all the time) <- we'll actually try to keep it between output phases and only reallocate if it is too small
      		int my_buffer_size = get_output_buffer_dimensions(MPMX, MPMY, ZMAX0 - (ZMIN-DELTA));
		my_buffer_size = my_buffer_size *three_floats;
		if(my_buffer_size > size_mpiio_surface_buf)
		{
			if(mpiio_surface_buffer)
				free(mpiio_surface_buffer);
			mpiio_surface_buffer = malloc(my_buffer_size);
			size_mpiio_surface_buf = my_buffer_size;
			if(!mpiio_surface_buffer)
			{
				fprintf(stderr, "Error: cannot allocate buffer for MPI-IO surface output optimization, will use no optimization instead\n");
				PRM.optSurface[0] = '\0';
				sprintf(PRM.optSurface, "none");
				//if we were "none" since the beginning, we would have allocated this so we needto do it know otherwise we'll get nasty errors
				coords_global[0] = malloc(sizeof(int)*1024);
				coords_global[1] = malloc(sizeof(int)*1024);
			}
		}
	} //end if MPI-IO surface output
    	else if(!strcmp("parallel", PRM.optSurface))
    	{   
  		jcpu = PRM.j2jcpu_array[OUT.j0]; //this is the fixed y used to write xz
  	  	icpu = PRM.i2icpu_array[OUT.i0]; //this is the fixed x used to write yz
     
	    	//in the first surface output phase we need to decide who writes each file
    		if(l == PRM.surfout)
	    	{
    			writer_xy[0] = 0; //rank 0 writes the first xy file
    			writer_xz[0] = 0;
	    		writer_yz[0] = 0;
    			writer_xy[1] = 0;
    			writer_xz[1] = 0;
	    		writer_yz[1] = 0;
	    		if(np > 1) //only makes sense if we are using multiple processes
    			{
    				int tmp_rank;
    				//we'll get the first process in xz which is not involved in yz to write the first xz
    				writer_xz[0] = jcpu*PRM.px; //if we can't choose, we get the default one
    				for(tmp_rank = 1; tmp_rank < np; tmp_rank++) //we avoid 0 because 0 is writing velocity xy
    					if((coords_global[1][tmp_rank] == jcpu) && (coords_global[0][tmp_rank] != icpu)) //this process is involved in xz AND this process is NOT involved in yz
	    				{
    						writer_xz[0] = tmp_rank;
    						break;
    					}

	    			//we'll get the first process in yz which is not involved in xz to write the first yz
    				writer_yz[0] = icpu;
    				for(tmp_rank = 1; tmp_rank < np; tmp_rank++)
    					if((coords_global[0][tmp_rank] == icpu) && (coords_global[1][tmp_rank] != jcpu))
    					{
    						writer_yz[0] = tmp_rank;
	    					break;
    					}
	    			//now we'll do the same to decide who write displacement yz and xz, but we try not to get the same ones from velocity
    				writer_xz[1] = jcpu*PRM.px; //if we can't choose, we get the default one
    				for(tmp_rank = 1; tmp_rank < np; tmp_rank++)
    					if((coords_global[1][tmp_rank] == jcpu) && (coords_global[0][tmp_rank] != icpu) && (tmp_rank != writer_xz[0])) 
    					{
    						writer_xz[1] = tmp_rank;
	    					break;
    					}
    				writer_yz[1] = icpu;
    				for(tmp_rank = 1; tmp_rank < np; tmp_rank++)
    					if((coords_global[0][tmp_rank] == icpu) && (coords_global[1][tmp_rank] != jcpu) && (tmp_rank != writer_yz[0]))
	    				{
    						writer_yz[1] = tmp_rank;
    						break;
    					}
	    			//now we get a completely different one for displacement xy
    				writer_xy[1] = 0;
    				for(tmp_rank = 1; tmp_rank < np; tmp_rank++)
    					if ((tmp_rank != writer_xz[0]) && (tmp_rank != writer_yz[0]) && (tmp_rank != writer_xz[1]) && (tmp_rank != writer_yz[1]))
    					{
    						writer_xy[1] = tmp_rank;
	    					break;
    					}
	    			// printf("Rank %d will write surface velocity xy, rank %d will write velocity xz, rank %d will write velocity yz, rank %d will write displacement xy, rank %d displacement xz, and rank %d displacement yz.\n", writer_xy[0], writer_xz[0], writer_yz[0], writer_xy[1], writer_xz[1], writer_yz[1]);
    			} //end if np > 1
    		} //end if this is the first time we'll write surface files in this execution
		//now we do the communication step for all files
		for ( snapStep = 1; snapStep <= snapStepMax; snapStep++ ){ //two steps maximum
			//figure what type of file we are writing in this step
		        int outvel=0, outdisp=0;     /* 0: no, 1: yes */
        		if ( (snapType == OVELO ) || (snapType == OBOTH && snapStep == 1)){
				outvel=1;
	        	} else if ((snapType == ODISPL) || (snapType == OBOTH && snapStep == 2)){
			        outdisp=1;
	        	}
			write_surface_files_parallel_communicationstep(&PRM, &OUT, &v0, outvel, outdisp, surface, model, writer_xy[snapStep-1], writer_xz[snapStep-1], writer_yz[snapStep-1], parallel_surface_buf[snapStep-1], np, coords_global, l, comm2d);
		} //end for 1 or 2 types of files
	 } //end if using parallel optimization for surface output

	for ( snapStep = 1; snapStep <= snapStepMax; snapStep++ ){ //two steps maximum
		//figure what type of file we are writing in this step
	        int outvel=0, outdisp=0;     /* 0: no, 1: yes */
        	if ( (snapType == OVELO ) || (snapType == OBOTH && snapStep == 1)){
			outvel=1;
        	} else if ((snapType == ODISPL) || (snapType == OBOTH && snapStep == 2)){
		        outdisp=1;
        	}
		//we'll output three planes:
		// k = 1 ou k0
		// j = j0
		// i = i0
		if(!strcmp("none", PRM.optSurface))
			write_surface_files_nooptimization(&PRM, &OUT, &v0, outvel, outdisp, surface, model, &local_buf, &write_buffer, np, coords_global, comm2d, l);
		else if(!strcmp("parallel", PRM.optSurface))
			write_surface_files_parallel_IOstep(&PRM, &OUT, &v0, outvel, outdisp, surface, model, &local_buf, &write_buffer, writer_xy[snapStep-1], writer_xz[snapStep-1], writer_yz[snapStep-1], parallel_surface_buf[snapStep-1], np, l);
		else if(!strcmp("MPIIO", PRM.optSurface))
			write_surface_files_mpiio(&PRM, &OUT, &v0, outvel, outdisp, surface, model, &local_buf, &write_buffer, x, y, np, mpiio_surface_buffer, comm2d, l);
		else
			fprintf(stderr, "Error: unknown surface output routine implementation %s\n", PRM.optSurface);
	} /* end for snapStep (maximum 2 steps, one for OVELO and another for ODISPL) */
#if (TIMER==2)
	MPI_Barrier (MPI_COMM_WORLD);
	if(PRM.me == 0)
		timing_IO_surface += my_second() - time_before_output;
#endif
  } //end surface output phase	




    /* ==================== END OF TIME LOOP ========================= */

    if ( (l % PRM.surfout) == 0 && PRM.me == 0 ){printf ("\nEnd time %d\n", l);}


  }	/* end time loop */

  /* ==================== END OF PROGRAM ========================= */
#ifdef MPI
#if (COMM==1)                /* close communications - deallocate requests */
  if ( NORTH.rank != -1 ) {
    MPI_Request_free(&reqT0S[1]); MPI_Request_free(&reqT0R[2]);
    MPI_Request_free(&reqV0S[1]); MPI_Request_free(&reqV0R[2]);
    if ( ANLmethod == KRISTEKandMOCZO ){
      MPI_Request_free(&reqKsiS[1]); MPI_Request_free(&reqKsiR[2]);
    }
  }
  if ( SOUTH.rank != -1 ) {
    MPI_Request_free(&reqT0R[1]); MPI_Request_free(&reqT0S[2]);
    MPI_Request_free(&reqV0R[1]); MPI_Request_free(&reqV0S[2]);
    if ( ANLmethod == KRISTEKandMOCZO ){
      MPI_Request_free(&reqKsiR[1]); MPI_Request_free(&reqKsiS[2]);
    }
  }

  if ( EAST.rank != -1 ) {
    MPI_Request_free(&reqT0S[3]); MPI_Request_free(&reqT0R[4]);
    MPI_Request_free(&reqV0S[3]); MPI_Request_free(&reqV0R[4]);
    if ( ANLmethod == KRISTEKandMOCZO ){
      MPI_Request_free(&reqKsiS[3]); MPI_Request_free(&reqKsiR[4]);
    }
  }
  if ( WEST.rank != -1 ) {
    MPI_Request_free(&reqT0R[3]); MPI_Request_free(&reqT0S[4]);
    MPI_Request_free(&reqV0R[3]); MPI_Request_free(&reqV0S[4]);
    if ( ANLmethod == KRISTEKandMOCZO ){
      MPI_Request_free(&reqKsiR[3]); MPI_Request_free(&reqKsiS[4]);
    }
  }

#endif
#endif

#ifdef FLOPS
  if ( (retval=PAPI_flops( &real_time, &proc_time, &flpops, &mflops)) < PAPI_OK ){
    printf("retval: %d\n", retval);
    exit(EXIT_FAILURE);
  }
  printf("Mflops %f \n", mflops);
#endif

#ifdef MISS
  if ( retval=PAPI_stop(EventSet,values) != PAPI_OK )
    printf("ERROR stop \n");
  perc = (float)100.0*values[1]/values[0];
  printf("Cache Miss %f %d \n", perc, PRM.me);
  printf("Cycle %lld %d \n", values[2], PRM.me);
  printf("L3 MISS  %lld %d \n", values[1], PRM.me);
  printf("L3 acces  %lld %d \n", values[0], PRM.me);
#endif

  timing4 = my_second();
  timing_total = (timing4-timing3);

#ifdef MPI
  MPI_Reduce (&timing_bc1,&timing_bc1_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc1,&timing_bc1_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc1,&timing_sum1,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc2,&timing_sum2,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc2,&timing_bc2_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc2,&timing_bc2_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_total,&timing4,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
#else
	timing_bc1_max = timing_bc1;
	timing_bc1_min = timing_bc1;
	timing_bc2_max = timing_bc2;
	timing_bc2_min = timing_bc2;
	timing4 = timing_total;
#endif



  timing_bc_total = timing_bc1 + timing_bc2;
  timing_comm_total = timing_comm1 + timing_comm2;

  /* On prend le proc avec le plus gros ecart au niveau de bc */
 #ifdef MPI
  MPI_Reduce (&timing_bc_total,&timing_bc_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_bc_total,&timing_bc_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_comm_total,&timing_comm_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
  MPI_Reduce (&timing_comm_total,&timing_comm_min,1,MPI_DOUBLE,MPI_MIN,0,MPI_COMM_WORLD);
#else
	timing_bc_max = timing_bc_total;
	timing_bc_min = timing_bc_total;
	timing_comm_max = timing_comm_total;
	timing_comm_min = timing_comm_total;
#endif /*  MPI */

//   printf("Taille domaine x/y - num processeur %d %d %d \n",MPMX,MPMY,PRM.me);

  if ( PRM.me == 0 ){

    printf("%d %d %d \n",np,PRM.px,PRM.py);
    printf("Timing total %f \n",timing_total);
    printf("Timing compute max  %f \n",timing_bc_max);
    printf("Timing compute min  %f \n",timing_bc_min);
    printf("Timing comm max - attente + comm reelle  %f \n",timing_comm_max);
    printf("Timing comm min - comm relle  %f \n",timing_comm_min);
    printf("Timing IO %f for station files and %f for surface files, total of %f \n", timing_IO_station, timing_IO_surface, timing_IO_station + timing_IO_surface);
    printf("Part communication: %f \n",100*(timing_comm_min)/timing_total);
    printf("Part compute: %f \n",100*(timing_bc_min)/timing_total);
    printf("Ecart BC vs timing total %f \n",100*((timing_bc_max-timing_bc_min)/timing_total));
    printf("Ecart BC %f \n",100*((timing_bc_max-timing_bc_min)/timing_bc_max));
    printf("Ecart Min/Max BC1 %f \n",100*((timing_bc1_max-timing_bc1_min)/timing_bc1_max));
    printf("Ecart Min/Max BC2 %f \n",100*((timing_bc2_max-timing_bc2_min)/timing_bc2_max));
    printf("timing max - BC1  %f \n ",timing_bc1_max);
    printf("timing min - BC1  %f \n ",timing_bc1_min);
    printf("timing max - BC2  %f \n ",timing_bc2_max);
    printf("timing min - BC2  %f \n ",timing_bc2_min);

    fp5 = fopen (BENCHFILE, "w");
	local_buf[0]='\0';
	sprintf(local_buf, "NP %d PX %d PY %d \nTOTAL TIME %f \n%f \n%f \n%f \n%f \nI/O %f (station) + %f (surface) = %f \n",np,PRM.px,PRM.py,timing_total,timing_bc_max,timing_bc_min,timing_comm_max,timing_comm_min, timing_IO_station, timing_IO_surface, timing_IO_station + timing_IO_surface);
	buffer_to_file(fp5, &write_buffer, local_buf, strlen(local_buf));
	local_buf[0]='\0';
	sprintf(local_buf, "%f \n%f \n%f \n%f \n%f \n%f \n",100*(timing_comm_min)/timing_total,100*(timing_bc_min)/timing_total,100*((timing_bc_max-timing_bc_min)/timing_total),100*((timing_bc_max-timing_bc_min)/timing_bc_max),100*((timing_bc1_max-timing_bc1_min)/timing_bc1_max),100*((timing_bc2_max-timing_bc2_min)/timing_bc2_max));
	buffer_to_file(fp5, &write_buffer, local_buf, strlen(local_buf));
	flush_buffer_to_file(fp5, &write_buffer);
    fclose(fp5);

    /* Fin cout max comm */

  } /* end PRM.me */
  
#ifdef MPI
  MPI_Barrier (MPI_COMM_WORLD);
#endif

  printf("------------- FIN -------------- %d \n", PRM.me);
#ifdef MPI
  MPI_Barrier (MPI_COMM_WORLD);
#endif



#if (TIMER==2)
	#ifdef MPI
		MPI_Barrier (MPI_COMM_WORLD);
	#endif
  timing4 = my_second();
  timing_total = (timing4-timing3);
#endif

#if (TIMER==1)
  timing4 = my_second();
  timing_total = (timing4-timing3);
#endif

	#ifdef MPI
		MPI_Barrier (MPI_COMM_WORLD);
	#endif


  fStatus= DeallocateAll(PRM.statout,
                         &ANL, &ABC,  &SRC, &MDM,
                         &t0, &v0, &OUT,
                         &NORTH, &SOUTH, &EAST,  &WEST,
                         &PRM  );
  VerifFunction(fStatus,"desallocate all",PRM);


	//free allocated data structures
  if(parallel_surface_buf[0])
	free(parallel_surface_buf[0]); 
  if(parallel_surface_buf[1])
	free(parallel_surface_buf[1]); 
  if(local_buf)
	free(local_buf);
  if(coords_global[0])
	free(coords_global[0]);
  if(coords_global[1])
	free(coords_global[1]);
  if(files_per_process)
	free(files_per_process);
  if(mpiio_station_buffer)
	free(mpiio_station_buffer);
  if(mpiio_surface_buffer)
	free(mpiio_surface_buffer);
  if(writer_rank)
	free(writer_rank);
  cleanup_write_buffer(&write_buffer);

#ifdef MPI
  MPI_Finalize();
#endif
  return (EXIT_SUCCESS);

} /* end of programm */

/* =============== */
/* SMALL FUNCTIONS */
/* =============== */

int VerifFunction( int exitStatus, const char* msg, struct PARAMETERS PRM ){
  if ( exitStatus != EXIT_SUCCESS ){
    if ( PRM.me == 0 ) {fprintf(stderr,"%-50s [ERROR] with cpu %i \n", msg, PRM.me ); }
    exit ( EXIT_FAILURE);
  }else{
#if (VERBOSE > 0)
	if ( PRM.me == 0 ) {fprintf(stderr,"%-50s [DONE ]\n", msg );}
#endif
	return EXIT_SUCCESS ;
  }
}


double my_second()
{
  struct timeval tp;
  struct timezone tzp;
  int i;

  i = gettimeofday(&tp,&tzp);
  return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}


/* ========== */
/* COMMUNICATIONS RELATED */
/* ========== */

int SyncBufStress( struct STRESS *t0,
				   int mode, /*  0: send, 1: receive */
				   struct COMM_DIRECTION *DIR,
				   struct PARAMETERS PRM
				   )
{
  long int i; 			/* index for assert Verification */
  int imp,jmp,k; 			/* local coordinates */
  assert (mode == 0 || mode == 1);
  if (mode == 0){ 		/* send */
    if ( DIR->rank == -1  ){ return CPU_NO_SEND;}
    else {
      i = 0;
      for ( imp = DIR->iMinS; imp <= DIR->iMaxS; imp++){
		for ( jmp = DIR->jMinS; jmp <= DIR->jMaxS; jmp++){
		  for ( k = PRM.zMin - PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			DIR->bufT0S[6*i  ] = t0->xx[imp][jmp][k];
			DIR->bufT0S[6*i+1] = t0->yy[imp][jmp][k];
			DIR->bufT0S[6*i+2] = t0->zz[imp][jmp][k];
			DIR->bufT0S[6*i+3] = t0->xy[imp][jmp][k];
			DIR->bufT0S[6*i+4] = t0->xz[imp][jmp][k];
			DIR->bufT0S[6*i+5] = t0->yz[imp][jmp][k];

			i = i + 1;
		  }
		}
      }

    } /* end if CPU */
  } else if (mode == 1){ 		/* Receive */
    if ( DIR->rank == -1  ){ return CPU_NO_RECV;}
    else {
      i = 0;
      for ( imp = DIR->iMinR; imp <= DIR->iMaxR; imp++){
		for ( jmp = DIR->jMinR; jmp <= DIR->jMaxR; jmp++){
		  for ( k = PRM.zMin-PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			t0->xx[imp][jmp][k] = DIR->bufT0R[6*i  ] ;
			t0->yy[imp][jmp][k] = DIR->bufT0R[6*i+1] ;
			t0->zz[imp][jmp][k] = DIR->bufT0R[6*i+2] ;
			t0->xy[imp][jmp][k] = DIR->bufT0R[6*i+3] ;
			t0->xz[imp][jmp][k] = DIR->bufT0R[6*i+4] ;
			t0->yz[imp][jmp][k] = DIR->bufT0R[6*i+5] ;
			i = i +1 ;
		  }
		}
      }
    } /* end if CPU */
  } /* end mode */

  return EXIT_SUCCESS;

} /* end function */

int SyncBufVelocity(struct VELOCITY *v0,
					int mode, /*  0: send, 1: receive */
					struct COMM_DIRECTION *DIR,
					struct PARAMETERS PRM
					)
{
  long int i; 			/* index for assert Verification */
  int imp,jmp,k; 			/* local coordinates */

  assert (mode == 0 || mode == 1);


  if (mode == 0){ 		/* send */
    if ( DIR->rank == -1  ){ return CPU_NO_SEND;}
    else {
      i = 0;
      for ( imp = DIR->iMinS; imp <= DIR->iMaxS; imp++){
		for ( jmp = DIR->jMinS; jmp <= DIR->jMaxS; jmp++){
		  for ( k = PRM.zMin-PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			DIR->bufV0S[3*i  ] = v0->x[imp][jmp][k];
			DIR->bufV0S[3*i+1] = v0->y[imp][jmp][k];
			DIR->bufV0S[3*i+2] = v0->z[imp][jmp][k];

			i = i + 1;
		  }
		}
      }
    } /* end if CPU */
  } else if (mode == 1){ 		/* Receive */
    if ( DIR->rank == -1  ){ return CPU_NO_RECV;}
    else {
      i = 0;
      for ( imp = DIR->iMinR; imp <= DIR->iMaxR; imp++){
		for ( jmp = DIR->jMinR; jmp <= DIR->jMaxR; jmp++){
		  for ( k = PRM.zMin-PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			v0->x[imp][jmp][k] = DIR->bufV0R[3*i  ] ;
			v0->y[imp][jmp][k] = DIR->bufV0R[3*i+1] ;
			v0->z[imp][jmp][k] = DIR->bufV0R[3*i+2] ;

			i = i +1 ;
		  }
		}
      }

    } /* end if CPU */
  } /* end mode */

  return EXIT_SUCCESS ;
} /* end function  */

int SyncBufKsil(struct  ANELASTICITY *ANL,
				int mode, /*  0: send, 1: receive */
				struct COMM_DIRECTION *DIR,
				struct PARAMETERS PRM
				)
{
  long int i; 			/* index for assert Verification */
  int imp,jmp,k; 			/* local coordinates */

  assert (mode == 0 || mode == 1);

  i = 0;
  if (mode == 0){ 		/* send */
    if ( DIR->rank == -1  ){ return CPU_NO_SEND;}
    else {
      for ( imp = DIR->iMinS; imp <= DIR->iMaxS; imp++){
		for ( jmp = DIR->jMinS; jmp <= DIR->jMaxS; jmp++){
		  for ( k = PRM.zMin-PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			DIR->bufKsiS[6*i  ] = ANL->ksilxx[imp][jmp][k];
			DIR->bufKsiS[6*i+1] = ANL->ksilyy[imp][jmp][k];
			DIR->bufKsiS[6*i+2] = ANL->ksilzz[imp][jmp][k];
			DIR->bufKsiS[6*i+3] = ANL->ksilxy[imp][jmp][k];
			DIR->bufKsiS[6*i+4] = ANL->ksilxz[imp][jmp][k];
			DIR->bufKsiS[6*i+5] = ANL->ksilyz[imp][jmp][k];

			i = i + 1;
		  }
		}
      }
    } /* end if CPU */
  } else if (mode == 1){ 		/* Receive */
    if ( DIR->rank == -1  ){ return CPU_NO_RECV;}
    else {
      for ( imp = DIR->iMinR; imp <= DIR->iMaxR; imp++){
		for ( jmp = DIR->jMinR; jmp <= DIR->jMaxR; jmp++){
		  for ( k = PRM.zMin-PRM.delta; k <= PRM.zMax0; k++){
			assert ( i <= DIR->nmax );
			ANL->ksilxx[imp][jmp][k] = DIR->bufKsiR[6*i  ] ;
			ANL->ksilyy[imp][jmp][k] = DIR->bufKsiR[6*i+1] ;
			ANL->ksilzz[imp][jmp][k] = DIR->bufKsiR[6*i+2] ;
			ANL->ksilxy[imp][jmp][k] = DIR->bufKsiR[6*i+3] ;
			ANL->ksilxz[imp][jmp][k] = DIR->bufKsiR[6*i+4] ;
			ANL->ksilyz[imp][jmp][k] = DIR->bufKsiR[6*i+5] ;
			i = i +1 ;
		  }
		}
      }
    } /* end if CPU */
  } /* end mode */

  return EXIT_SUCCESS ;
} /* end function  */


int ComputeSeismograms( struct OUTPUTS *OUT, struct VELOCITY v0, struct STRESS t0, struct PARAMETERS PRM,int l )
{ int icpu,jcpu;
  int ir;
  int imp, jmp;
  int i0, j0, k0, i2, j2, k2; /* 0=here, 2=at +ds/2 */
  double wx0, wy0, wz0, wx2, wy2, wz2;
  double weight[3];
  double values[8];

  /* For info :
   * Vx component  : i, j , k
   * Vy component  : i2, j2 , k
   * Vz component  : i2, j
   * Tii component : i2, j
   * Txy component : i, j2
   * Txz component : i, j
   * Tyz component : i2, j2
   */

  for ( ir = 0; ir < OUT->iObs; ir++){
    if ( OUT->ista[ir] == 1 ){


      i0 = OUT->ixobs[ir];
      wx0 = OUT->xobswt[ir];
      j0 = OUT->iyobs[ir];
      wy0 = OUT->yobswt[ir];
      k0 = OUT->izobs[ir];
      wz0 = OUT->zobswt[ir];

      /* index and weight for +/-DS/2 component */
      if ( wx0>= 0.5 ){
        i2=i0;
        wx2 = wx0 - 0.5;
      }else{
        i2=i0-1;
        wx2 = wx0 + 0.5;
      }

      if ( wy0>= 0.5 ){
        j2=j0;
        wy2 = wy0 - 0.5;
      }else{
        j2=j0-1;
        wy2 = wy0 + 0.5;
      }

      if ( wz0>= 0.5 ){
        k2=k0;
        wz2 = wz0 - 0.5;
      }else{
        k2=k0-1;
        wz2 = wz0 + 0.5;
      }
      /* correct index on the last cell on the free surface */
      if ( surface == FREE ){
		if ( OUT->izobs[ir] == 1 ){
		  wz2 = 0.0;
		  k2 = 0;
		}
      }

      /* Vx component */
      if ( PRM.me ==  OUT->mapping_seis[ir][1] ){
		imp = PRM.i2imp_array[i0];
		jmp = PRM.j2jmp_array[j0];


		weight[0]=wx0;	weight[1]=wy0;	weight[2]=wz0;

		values[0]= v0.x[imp][jmp][k0]; values[1] = v0.x[imp+1][jmp][k0];
		values[2]= v0.x[imp][jmp+1][k0]; values[3] = v0.x[imp+1][jmp+1][k0];

		values[4]= v0.x[imp][jmp][k0+1]; values[5] = v0.x[imp+1][jmp][k0+1];
		values[6]= v0.x[imp][jmp+1][k0+1]; values[7] = v0.x[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][1]  = Weight3d( weight, values );


      }

      /* Vy component */
      if ( PRM.me ==  OUT->mapping_seis[ir][2] ){
		imp = PRM.i2imp_array[i2];
		jmp = PRM.j2jmp_array[j2];
		weight[0]=wy2;	weight[1]=wx2;	weight[2]=wz0;

		values[0]= v0.y[imp][jmp][k0]; values[1] = v0.y[imp][jmp+1][k0];
		values[2]= v0.y[imp+1][jmp][k0]; values[3] = v0.y[imp+1][jmp+1][k0];

		values[4]= v0.y[imp][jmp][k0+1]; values[5] = v0.y[imp][jmp+1][k0+1];
		values[6]= v0.y[imp+1][jmp][k0+1]; values[7] = v0.y[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][2]  = Weight3d( weight, values );
      }

      /* Vz component */
      if ( PRM.me ==  OUT->mapping_seis[ir][3] ){
		imp = PRM.i2imp_array[i2];
		jmp = PRM.j2jmp_array[j0];
		weight[0]=wy0;	weight[1]=wx2;	weight[2]=wz2;

		values[0]= v0.z[imp][jmp][k2]; values[1] = v0.z[imp][jmp+1][k2];
		values[2]= v0.z[imp+1][jmp][k2]; values[3] = v0.z[imp+1][jmp+1][k2];

		values[4]= v0.z[imp][jmp][k2+1]; values[5] = v0.z[imp][jmp+1][k2+1];
		values[6]= v0.z[imp+1][jmp][k2+1]; values[7] = v0.z[imp+1][jmp+1][k2+1];

		OUT->seis_output[l%PRM.statout][ir][3]  = Weight3d( weight, values );
      }

      /* Tii component i2, j, k */
      if ( PRM.me ==  OUT->mapping_seis[ir][4] ){
		/*  5 and 6 are the same because tii are at the same cell*/
		imp = PRM.i2imp_array[i2];
		jmp = PRM.j2jmp_array[j0];
		weight[0]=wy0;	weight[1]=wx2;	weight[2]=wz0;
		/* txx */
		values[0]= t0.xx[imp][jmp][k0]; values[1] = t0.xx[imp][jmp+1][k0];
		values[2]= t0.xx[imp+1][jmp][k0]; values[3] = t0.xx[imp+1][jmp+1][k0];

		values[4]= t0.xx[imp][jmp][k0+1]; values[5] = t0.xx[imp][jmp+1][k0+1];
		values[6]= t0.xx[imp+1][jmp][k0+1]; values[7] = t0.xx[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][4]  = Weight3d( weight, values );
		/* tyy */
		values[0]= t0.yy[imp][jmp][k0]; values[1] = t0.yy[imp][jmp+1][k0];
		values[2]= t0.yy[imp+1][jmp][k0]; values[3] = t0.yy[imp+1][jmp+1][k0];

		values[4]= t0.yy[imp][jmp][k0+1]; values[5] = t0.yy[imp][jmp+1][k0+1];
		values[6]= t0.yy[imp+1][jmp][k0+1]; values[7] = t0.yy[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][5]  = Weight3d( weight, values );
		/* tzz */
		values[0]= t0.zz[imp][jmp][k0]; values[1] = t0.zz[imp][jmp+1][k0];
		values[2]= t0.zz[imp+1][jmp][k0]; values[3] = t0.zz[imp+1][jmp+1][k0];

		values[4]= t0.zz[imp][jmp][k0+1]; values[5] = t0.zz[imp][jmp+1][k0+1];
		values[6]= t0.zz[imp+1][jmp][k0+1]; values[7] = t0.zz[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][6]  = Weight3d( weight, values );
      }

      /* Txy component i, j2, k */
      if ( PRM.me ==  OUT->mapping_seis[ir][7] ){
		imp = PRM.i2imp_array[i0];
		jmp = PRM.j2jmp_array[j2];
		weight[0]=wy2;	weight[1]=wx0;	weight[2]=wz0;

		values[0]= t0.xy[imp][jmp][k0]; values[1] = t0.xy[imp][jmp+1][k0];
		values[2]= t0.xy[imp+1][jmp][k0]; values[3] = t0.xy[imp+1][jmp+1][k0];

		values[4]= t0.xy[imp][jmp][k0+1]; values[5] = t0.xy[imp][jmp+1][k0+1];
		values[6]= t0.xy[imp+1][jmp][k0+1]; values[7] = t0.xy[imp+1][jmp+1][k0+1];

		OUT->seis_output[l%PRM.statout][ir][7]  = Weight3d( weight, values );
      }

      /* Txz component i, j, k2 */
  
    if ( PRM.me ==  OUT->mapping_seis[ir][8] ){
		imp = PRM.i2imp_array[i0];
		jmp = PRM.j2jmp_array[j0];
		weight[0]=wy0;	weight[1]=wx0;	weight[2]=wz2;

		values[0]= t0.xz[imp][jmp][k2]; values[1] = t0.xz[imp][jmp+1][k2];
		values[2]= t0.xz[imp+1][jmp][k2]; values[3] = t0.xz[imp+1][jmp+1][k2];

		values[4]= t0.xz[imp][jmp][k2+1]; values[5] = t0.xz[imp][jmp+1][k2+1];
		values[6]= t0.xz[imp+1][jmp][k2+1]; values[7] = t0.xz[imp+1][jmp+1][k2+1];

		OUT->seis_output[l%PRM.statout][ir][8]  = Weight3d( weight, values );
      }

      /* Tyz component i2, j2, k2 */
      if ( PRM.me ==  OUT->mapping_seis[ir][9] ){
		imp = PRM.i2imp_array[i2];
		jmp = PRM.j2jmp_array[j2];
		weight[0]=wy2;	weight[1]=wx2;	weight[2]=wz2;

		values[0]= t0.yz[imp][jmp][k2]; values[1] = t0.yz[imp][jmp+1][k2];
		values[2]= t0.yz[imp+1][jmp][k2]; values[3] = t0.yz[imp+1][jmp+1][k2];

		values[4]= t0.yz[imp][jmp][k2+1]; values[5] = t0.yz[imp][jmp+1][k2+1];
		values[6]= t0.yz[imp+1][jmp][k2+1]; values[7] = t0.yz[imp+1][jmp+1][k2+1];

		OUT->seis_output[l%PRM.statout][ir][9]  = Weight3d( weight, values );
      }


	}	/* end station is inside the domain */
  } /* end ir */

  return EXIT_SUCCESS  ;

} /* end function */

double Weight3d( double w[3],	/* weights */
				 double v[8] 	/* values */
				 )
{
  double result;

  result =
	( 1. - w[2])*(
				  ( 1. - w[1])* ( (1.-w[0])*v[0] + w[0]*v[1] ) +
				  ( w[1]     )* ( (1.-w[0])*v[2] + w[0]*v[3] )
				  ) +
	(  w[2]    )*(
				  ( 1. - w[1])* ( (1.-w[0])*v[4] + w[0]*v[5] ) +
				  ( w[1]     )* ( (1.-w[0])*v[6] + w[0]*v[7] )
				  )  ;

  return result;
}


//we allocate a single buffer to write xy, xz and yz files. This function receives the sizes of these dimensions and returns the number of cells in the buffer
int get_output_buffer_dimensions(int size_x, int size_y, int size_z)
{
	if(size_x >= size_y) //so xz is larger than yz
	{
		if(size_y > size_z) //so xy is larger than xz
			return size_x*size_y;
		else //xz is the largest
			return size_x*size_z;
	}
	else // yz is larger than xz
	{
		if(size_x > size_z) // xy is larger than yz
			return size_x*size_y;
		else //yz is the largest
			return size_y*size_z;
	}
}
