CC		=	mpicc

#List of Path to search sources files
VPATH		= .:../src
#Ansi conformity
#TESTFLAGS	+= -Xs

#######################################################
## DEBUG PARAMETERS
TESTFLAGS       += -DVERBOSE=0

# lecture
#TESTFLAGS	+= -DDEBUG_READ
# allocation  
#TESTFLAGS	+= -DDEBUG_ALLO
# no velocity computation
#TESTFLAGS      += -DNOVELOCITY
# no stress computation
#TESTFLAGS      += -DNOSTRESS
# no intermediates computation
#TESTFLAGS      += -DNOINTERMEDIATES
# no absorbing condition computation
#TESTFLAGS       += -DNOABS
# no anelasticity computation
#TESTFLAGS      += -DNOANELASTICITY
###################################################

####################################################
#EXECUTION FLAG
TESTFLAGS	+= -DMPI
MPI_FLAGS	=	
TESTFLAGS	+= -DOMP
OMP_FLAGS	= -fopenmp
# COMM=1 : persistant / COMM=2 : blocking
TESTFLAGS	+= -DCOMM=1
# with MPI topologie file (topologie.in)
#TESTFLAGS	+= -DTOPO_MPI
#####################################################





####################################################
#PROFILING FLAG

# TIMER=1 : standard / TIMER = 2 : timer vith MPI barriers
TESTFLAGS       += -DTIMER=2
#TAU detailed profiling
#TESTFLAGS	+= -DPROFILE1
#TAU global profiling
#TESTFLAGS       += -DPROFILE2
#FLOPS based on top of PAPI library
#TESTFLAGS       += -DFLOPS
#PAPI counters (cache misses) using PAPI library
#TESTFLAGS       += -DMISS

#######################################################
# OUTPUT
#Write VTK files
TESTFLAGS	+= -DVTK
#Write geological model
#TESTFLAGS	+= -DOUT_HOGE

########################################################



#########################################################"
## OPTIMISATION PARAMETERS
OPTI 		+=  -O2 
###########################################################


#MODEL parameters; default values are in options.h
CFLAGS		=   $(TESTFLAGS)  $(OPTI)  $(MODEL) 
PREFIX =ondes3d$(POST)
OBJS = main.o nrutil.o  computeVeloAndSource.o computeStress.o computeIntermediates.o alloAndInit.o IO.o alloAndInit_LayerModel.o surface_output.o write_buffer.o station_output.o mpiio_common.c
HEADERS=struct.h inlineFunctions.h options.h 


all: $(PREFIX)
$(PREFIX): $(OBJS)
	$(CC) $(MPI_FLAGS) $(OMP_FLAGS) -o $@ $^ -lm 
%.o: %.c
	$(CC) $(MPI_FLAGS) $(OMP_FLAGS) $(CFLAGS) -c $<

clean:
	rm -f *.o  *~ ondes3d
