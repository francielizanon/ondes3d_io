/* station_output.h
 * Author: Francieli Zanon Boito
 * this file provides headers for the three available implementation of the station files output routine
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 * options are: "none" (the original implementation), "parallel" (in the paper called "distributed station output") and "MPIIO"
 */

#ifndef _STAITON_OUTPUT_
#define _STAITON_OUTPUT_

#include "write_buffer.h"
#include "struct.h"

void write_station_files_nooptimization(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, MPI_Comm comm2d, int l, struct write_buffer_t *write_buffer, char *local_buf);
void write_station_files_mpiio(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, MPI_Comm comm2d, char *local_buf, char *mpiio_station_buffer, int l);
void write_station_files_parallel_communicationstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int *writer_rank, int *files_per_process, int ir, MPI_Comm comm2d);
void write_station_files_parallel_IOstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, int ir, int l, char *local_buf, struct write_buffer_t *write_buffer);
int get_dat_writer_rank(struct OUTPUTS *OUT, int ir, int *files_per_process);
int am_i_involved_in_dat(struct OUTPUTS *OUT, int ir, int me);
int get_my_dat_field(struct OUTPUTS *OUT, int ir, int me, int first_field, int *size, int *start);


#endif
