/* write_buffer.c
 * Author: Francieli Zanon Boito
 * this file provides headers for the implementation of the software layer used to stage and aggregate requests to files
 * this optimization is discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 */

#ifndef _WRITE_BUFFER_H_
#define _WRITE_BUFFER_H_

#include <stdio.h>
#include <stdlib.h>

struct write_buffer_t {
	char *buf;
	int index;
	int size;
};
void init_write_buffer(struct write_buffer_t *write_buffer);
int allocate_write_buffer(struct write_buffer_t *write_buffer, int new_size); //returns 0 on success
void buffer_to_file(FILE *fp, struct write_buffer_t *write_buffer, char *req, int req_size);
void flush_buffer_to_file(FILE *fp, struct write_buffer_t *write_buffer);
void write_float(FILE *fp, float val, struct write_buffer_t *write_buffer);

void cleanup_write_buffer(struct write_buffer_t *write_buffer);
#endif
