﻿***************************************************
**   About				         **
***************************************************

This repository contains the Ondes3D 1.1 application for seismic simulations with modifications made to its output routines. These optimizations are discussed in the PDP 2017 paper "High Performance I/O for Seismic Wave Propagation Simulations". The paper is available at: https://lume.ufrgs.br/handle/10183/159549

The application repository can be accessed at: https://bitbucket.org/fdupros/ondes3d/

The new output implementations are mostly in the surface_output.\*, station_output.\*, mpiio_common.\*, and write_buffer.\* files. If you encounter problems with these implementations, please open an issue here or contact Francieli Zanon Boito.

***************************************************
**   Instructions to run		         **
***************************************************

The repository includes the use case from the PDP 2017 paper in the NICE-XML folder. 
To execute it:

* create a folder (for instance, "NICE-OUTPUT") to where you want the application to write data (for instance, to a parallel file system). Make sure the folder is empty before running each instance of the application otherwise it will crash.

* Place the NICE-XML folder in a location from where you want the application to read data (for instance, in a parallel file system).

* Edit the nice.prm file (in the NICE-XML folder) to configure the simulation:
	- "tmax" gives the number of time steps to be simulated;
	- "dir" is the output folder you created;
	- "statout" is the number of time steps between consecutive station output routines;
	- "surfout" is the number of time steps between consecutive surface output routines;
	- "bufSize" is the size of the buffer that will be allocated to stage and aggregate write requests to files (in KB);
	- "statOpt" is the implementation of the station output routine that is going to be used (options are "none", "parallel", and "MPIIO");
	- "surfOpt" is the implementation of the surface output routine that is going to be used (options are "none", "parallel", and "MPIIO");
	- "fstatMap", "fsrcMap", "fsrcHist", and "fgeo" need to be updated to contain the correct path to the input files in the NICE-XML folder.

* In the main.c file, line 53, change the path where the "bench.out" file will be written at the end of the execution. It will show measurements of time spent on different activities, but these measurements (specially for I/O) will only be accurate if the "TIMER" option in the Makefile is set to 2. Therefore, if you wish to gather time spent on I/O operations, make sure TIMER is set to 2 in the Makefile. Nevertheless, it is important to notice using TIMER=2 imposes new MPI_Barrier calls, and thus is not guaranteed to reproduce the results presented in the paper, which were obtained without it (time spent in I/O was obtained from the Darshan profiling tool). 

* In the options.h file, give the full path to the nice.prm file in your NICE-XML folder. 

* If you do not want to compile with OpenMP, you can comment the relevant lines in the Makefile.  

* Compile Ondes3D with make clean and then make .

* Run with mpirun -np (number of MPI processes) ./ondes3d (number of OMP threads per MPI process) .

***************************************************
**   Credits				         **
***************************************************

Work partially funded by CAPES-Brazil, conducted at the Federal University of Santa Catarina and at the Federal University of Rio Grande do Sul.

