/* surface_output.c
 * Author: Francieli Zanon Boito
 * this file provides the three available implementation of the surface files output routine
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 * options are: "none" (the original implementation), "parallel" (in the paper called "distributed surface output") and "MPIIO"
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "surface_output.h"
#include "nrutil.h"
#include "mpiio_common.h"

//------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------------
//this is the original implementation of the surface output routine, but changed to use the buffer to write to file (instead of generating tons of very small requests)
//I translated comments that were in French, added a few more to help understand the code, and I fixed a few bugs (Francieli) 
void write_surface_files_nooptimization(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int np, int **coords_global, MPI_Comm comm2d, int l)
{
  FILE *fp1, *fp2, *fp3;
  int i, j, k, i1, i2, imp, jmp_tmp, icpu, imp_tmp, jcpu;
  MPI_Status status;
  MPI_Request sendreq[4];
  int     proc_coords[2];
  char flname[80];

  /* ======================================================================================================= */
  /* Writing of surfacexy */
  /* ======================================================================================================= */
  if ( PRM->me == 0 ){ //rank 0 is the one who will write this file
	//allocate data structures to accumulate all data from myself and the other processes, and then copy my data into it
	OUT->Vxglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        OUT->Vyglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        OUT->Vzglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);

        for ( i = 1; i <= PRM->mpmx; i++){
        	for ( j = 1; j <= PRM->mpmy; j++){
		        if (outvel == 1){
              			if(surface == ABSORBING || model == GEOLOGICAL){
			                OUT->Vxglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->x[i][j][OUT->k0];
			                OUT->Vyglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->y[i][j][OUT->k0];
			                OUT->Vzglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->z[i][j][OUT->k0 -1];
			         } else if (surface == FREE && model != GEOLOGICAL){ /* free surface at z = 0 */
			                OUT->Vxglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->x[i][j][1];
			                OUT->Vyglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->y[i][j][1];
			                OUT->Vzglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = v0->z[i][j][0];
			         } /* end of if surface */
		        } /* end if outvel == 1 */
			else  if (outdisp == 1 ){
			        OUT->Vxglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = OUT->Uxy[1][i][j];
		                OUT->Vyglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = OUT->Uxy[2][i][j];
		                OUT->Vzglobal[PRM->xMin-PRM->delta+i-1][PRM->yMin-PRM->delta+j-1] = OUT->Uxy[3][i][j];
            		} /* end if snapType */
          	} /* end for j */
        } /* end of i */
  } /* end of PRM->me = 0 */

  for (i1 = 1; i1 <= 4; i1++){
	if ( PRM->me != 0 ){ //four steps if I'm not rank 0  (all processes are involved in this file, no exceptions)
        	if ( i1 == 1 ){ //step 1: send my coordinates to rank 0
           		MPI_Isend (PRM->coords,2,MPI_INT, 0,90,comm2d,&sendreq[0]);
		        MPI_Wait (&sendreq[0], &status) ;
	        } /* end of step i1 = 1 */
	        if ( i1 == 2 ){ //step 2: copy x data to my snapBuff and send it to rank 0
        		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
                			if (outvel == 1){
				                if (surface == ABSORBING ){ OUT->snapBuff[imp] = v0->x[i][j][OUT->k0];
				                } else if (surface == FREE ){OUT->snapBuff[imp] = v0->x[i][j][1];
						}
				        } else if (outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxy[1][i][j];
                			}
		                 } /* end for j */
		        } /* end for i */
		        MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,0,80,comm2d,&sendreq[1]);
		        MPI_Wait (&sendreq[1], &status);
	        } /* end of step i1 = 2 */
	        if ( i1 == 3 ){ //step 3: copy y data to my snapBuff and send it to rank 0
            		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
              			for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if (outvel == 1){
                  				if (surface == ABSORBING ){ OUT->snapBuff[imp] = v0->y[i][j][OUT->k0];
						} else if ( surface == FREE ){OUT->snapBuff[imp] = v0->y[i][j][1];
						}
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxy[2][i][j];
			                }
		                 } /* end for j */
		        } /* end for i */
          	  	MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,0,81,comm2d,&sendreq[2]);
            		MPI_Wait (&sendreq[2], &status);
          	} /* end of step i1 = 3 */
	        if ( i1 == 4 ){
         		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){ //step 4 send z data to my snapBuff and send it to rank 0
			        for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                if ( surface == ABSORBING ){ /* absorbing layer above z = PRM->zMax */
					                OUT->snapBuff[imp] = v0->z[i][j][OUT->k0];
				                 } else if ( surface == 1 ){ /* free surface at z = 0 */
					                 OUT->snapBuff[imp] = v0->z[i][j][0];
				                 } /* end of if surface */
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxy[3][i][j];
			                }
			        } /* end for j */
		        } /* end for i */
          		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,0,82,comm2d,&sendreq[3]);
		        MPI_Wait (&sendreq[3], &status);
	        } /* end of step i1 = 4 */
	} else { //four steps if I AM rank 0
		for ( i2 = 1; i2 < np; i2++){ //we'll do these steps for each of the other processes
		        if ( i1 == 1 ){ //step 1: receive coordinates
				coords_global[0][i2] = 0;
			        coords_global[1][i2] = 0;
            			MPI_Recv (proc_coords,2,MPI_INT,i2,90,comm2d,&status);
			        coords_global[0][i2] = proc_coords[0];
        			coords_global[1][i2] = proc_coords[1];
		        } /* end of step i1 = 1 */
		        if ( i1 == 2 ){ //step 2: receives x data
	  		        MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,80, comm2d,&status);
		                OUT->total_prec_x = 0;
			        for (j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x += PRM->mpmx_tab[j];
		                }
			        OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y += PRM->mpmy_tab[j];
			        }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                OUT->Vxglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
		                 } /* end for i */
            		} /* end of step i1 = 2 */
		        if ( i1 == 3 ){ //step 3: receive y data
				MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,81, comm2d,&status);
		                OUT->total_prec_x = 0;
		                for ( j = 0; j < coords_global[0][i2];j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
			        }
		                OUT->total_prec_y = 0;
		                for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
		 	               for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				               assert (imp < OUT->test_size) ;
				               assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				               assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				               imp ++;
				               OUT->Vyglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
		                } /* end for i */
		        } /* end of step i1 = 3 */
		        if ( i1 == 4 ){ //step 4: receive z data
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,82, comm2d,&status);
			        OUT->total_prec_x = 0;
				for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
		                OUT->total_prec_y = 0;
		                for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                OUT->Vzglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
			        } /* end for i */
		        } /* end of step i1 = 4 */
		} /* end of for all other processes */
	} /* end of if I'm rank 0 */
  } /* end of for i1 in steps 1 to 4 */

  //write the file (rank 0 will do that)
  if ( PRM->me == 0 ){
	int ndiv =1;
        double dssurf=PRM->ds*ndiv;

        int XMINS=(int) ceil((PRM->xMin-PRM->delta)/ndiv);
        int XMAXS=(int) floor((PRM->xMax+PRM->delta)/ndiv);
        int dimx= XMAXS-XMINS+1;
        int YMINS=(int) ceil((PRM->yMin-PRM->delta)/ndiv);
        int YMAXS=(int) floor((PRM->yMax+PRM->delta)/ndiv);
        int dimy= YMAXS-YMINS+1;
	
	flname[0] = '\0';
	sprintf(flname, "%ssurfacexy%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp1 = fopen(flname, "w");

        /* print VTK header*/
	//if I don't have local_buf, need to allocate it
	if((*local_buf) == NULL)
	{
		*local_buf = malloc(sizeof(char)*500);
		if((*local_buf) == NULL)
		{
			fprintf(stderr, "PANIC! Could not allocate small buffer for writing to surface file, aborting the output routine\n");
			return;
		}
	}
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf)); 
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",dimx,dimy,1);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,  "ORIGIN %f %f %f\n",XMINS*dssurf,YMINS*dssurf,0.);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",dimx*dimy*1);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));

        for ( j = PRM->yMin-PRM->delta+1; j <= PRM->yMax+PRM->delta+1; j++ ){
          for ( i = PRM->xMin-PRM->delta+1; i <= PRM->xMax+PRM->delta+1; i++ ){
            if( ((i-1)%ndiv) == 0 && ((j-1)%ndiv) == 0 ){
              write_float(fp1,(float) OUT->Vxglobal[i][j], write_buffer);
              write_float(fp1,(float) OUT->Vyglobal[i][j], write_buffer);
              write_float(fp1,(float) OUT->Vzglobal[i][j], write_buffer);
            }
          }
        }
	flush_buffer_to_file(fp1, write_buffer); //we need to flush the buffer before closing the file
        fclose(fp1);
       
	//free the buffers used to accumulate all data (mine and from the other processes) 
        free_dmatrix(OUT->Vxglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        free_dmatrix(OUT->Vyglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        free_dmatrix(OUT->Vzglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
  } /* end of if PRM->me, wrote surfacexy */

  /* ======================================================================================================= */
  /* Writing of surfacexz */
  /* ======================================================================================================= */
  /* Look for the process with j0 and coords[0] = 0. We know its coordinates (0, jcpu), so rank = jcpu *px */
  /* that will be the rank to write the surfacexz file */
  jcpu = PRM->j2jcpu_array[OUT->j0];
  jmp_tmp =  PRM->j2jmp_array[OUT->j0];

  if ( PRM->me  == jcpu*PRM->px ){ //if I'm the rank that will write this file
	//allocate data structures that will be filled with data from other processes and then written
        OUT->Vxglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        OUT->Vyglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        OUT->Vzglobal = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);

	//copy my data to the data structures (the rest will come from the other processes 
        for ( i = 1; i <= PRM->mpmx; i++){
        	for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
		        if ( outvel == 1){
		 	       OUT->Vxglobal[PRM->xMin-PRM->delta+i-1][k] = v0->x[i][jmp_tmp][k];
		               OUT->Vyglobal[PRM->xMin-PRM->delta+i-1][k] = v0->y[i][jmp_tmp][k];
		               OUT->Vzglobal[PRM->xMin-PRM->delta+i-1][k] = v0->z[i][jmp_tmp][k];
		        } else if ( outdisp == 1 ){
			       OUT->Vxglobal[PRM->xMin-PRM->delta+i-1][k] = OUT->Uxz[1][i][k];
			       OUT->Vyglobal[PRM->xMin-PRM->delta+i-1][k] = OUT->Uxz[2][i][k];
		               OUT->Vzglobal[PRM->xMin-PRM->delta+i-1][k] = OUT->Uxz[3][i][k];
		        }
	        }
        }
  } //end if I'm the rank that will write
  for ( i1 = 1; i1 <=4; i1++){ //four steps
	  if ( (PRM->coords[1] == jcpu) && (PRM->me != PRM->px*jcpu) ){ //if I'm not the writer rank, but I'm invovled in this file (not everyone will be) I'll send data to the writer rank in four steps
	          if ( i1 == 1 ){ //step 1: send my coordinates
        	  	MPI_Isend (PRM->coords,2,MPI_INT,jcpu*PRM->px,90,comm2d,&sendreq[0]);
		        MPI_Wait (&sendreq[0], &status);		
	          } /* end of i1 = 1 */
        	  if ( i1 == 2 ){ //step 2: send my x data
		          imp = 0;
		          for ( i = 1; i <= PRM->mpmx; i++){
              		  	for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size);
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->x[i][jmp_tmp][k];
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxz[1][i][k];
			                }
			         } /*end for k*/
		          } /* end for i */
		          MPI_Isend(OUT->snapBuff,OUT->test_size,MPI_DOUBLE,jcpu*PRM->px,80,comm2d,&sendreq[1]);
		          MPI_Wait (&sendreq[1], &status) ;
	          } /* end of step i1 = 2 */
	          if ( i1 == 3 ){ //step 3: send my y data
			imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
	  	              for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			              assert (imp < OUT->test_size);
			              imp ++;
			              if ( outvel == 1){
				              OUT->snapBuff[imp] = v0->y[i][jmp_tmp][k];
			              } else if ( outdisp == 1 ){
				              OUT->snapBuff[imp] = OUT->Uxz[2][i][k];
			              }
		              } /* end for k*/
		        } /*end for i */
		        MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,jcpu*PRM->px,81,comm2d,&sendreq[2]);
		        MPI_Wait (&sendreq[2], &status);
          	  } /* end of step i1 = 3 */
	          if ( i1 == 4 ){ //step 4: send my z data
		          imp = 0;
		          for ( i = 1; i <= PRM->mpmx; i++){
			          for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
		  	                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->z[i][jmp_tmp][k];
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxz[3][i][k];
			                }
			           } /*end for k */
            		   } /*end for i*/
            		   MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,jcpu*PRM->px,82,comm2d,&sendreq[3]);
		           MPI_Wait (&sendreq[3], &status);
          	  } /* end of step i1 = 4 */
	} /* end of if I'm involved but I'm not the writer */
        if ( PRM->me == jcpu*PRM->px ){ // if I'm the writer of this file
	        for (i2 = 1; i2 < PRM->px ; i2++){ //all do the four steps to each of the other processes involved in this file
		        if ( i1 == 1 ){ //step 1: get coordinates
              			MPI_Recv (proc_coords,2,MPI_INT,jcpu*PRM->px+i2,90,comm2d,&status);
			        coords_global[0][i2] = proc_coords[0];
		                coords_global[1][i2] = proc_coords[1];
		        } /* end of step i1 = 1 */
		        if ( i1 == 2 ){ //step 2: receive x data
				MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,80, comm2d,&status);
		                OUT->total_prec_x = 0;
		                for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                imp ++;
				                OUT->Vxglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
                			} /*end for k */
		                 } /*end for i */
		        } /* end of step i1 = 2 */
		        if ( i1 == 3 ){ //step 3: receive y data
	  	              MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,81, comm2d,&status);
		              OUT->total_prec_x = 0;
		              for ( j = 0; j < coords_global[0][i2]; j++){
			              OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		              }
		              imp = 0;
		              for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			              for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				              assert (imp < OUT->test_size) ;
				              assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				              imp ++;
				              OUT->Vyglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
			                } /*end for k*/
		              } /*end for i*/
	                } /* end of step i1 = 3 */
		        if ( i1 == 4 ){ //step 4: receive z data
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,82, comm2d,&status);
			        OUT->total_prec_x = 0;
		                for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
			        imp = 0;
			        for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                imp ++;
				                OUT->Vzglobal[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
			                } /*end for k*/
			        } /* end for i*/
		        } /* end of step i1 = 4 */
	          } /* end for all other processes */
  	} /* end of if I'm the writer rank*/
  } /* end of for i1 in four steps*/

  //now write the file, only the writer rank will do that
  /* Warning :
     - for Free Surface, we do not write k=2
     - for the rest, we do not write the last cell
  */
  if ( PRM->me == jcpu*PRM->px ){
        int ndiv =1;
        double dssurf=PRM->ds*ndiv;

        int XMINS=(int) ceil((PRM->xMin-PRM->delta)/ndiv);
        int XMAXS=(int) floor((PRM->xMax+PRM->delta)/ndiv);
        int dimx= XMAXS-XMINS+1;
        int ZMINS=(int) ceil((PRM->zMin-PRM->delta)/ndiv);
        int ZMAXS=(int) floor((1)/ndiv);
        int dimz= ZMAXS-ZMINS+1;

	flname[0] = '\0';
	sprintf(flname, "%ssurfacexz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp2 = fopen(flname, "w");

        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",dimx,1,dimz);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"ORIGIN %f %f %f\n",XMINS*dssurf,OUT->j0*PRM->ds,ZMINS*dssurf);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",dimx*dimz*1);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));

        for ( k = PRM->zMin-PRM->delta+1; k <= 2; k++ ){
          for ( i = PRM->xMin-PRM->delta+1; i <= PRM->xMax+PRM->delta+1; i++ ){
            if( ((i-1)%ndiv) == 0 && ((k-1)%ndiv) == 0 ){
              write_float(fp2,(float) OUT->Vxglobal[i][k], write_buffer);
              write_float(fp2,(float) OUT->Vyglobal[i][k], write_buffer);
              write_float(fp2,(float) OUT->Vzglobal[i][k], write_buffer);
            }
          }
        }
	flush_buffer_to_file(fp2, write_buffer);
        fclose(fp2);

	//free the buffers used to accumulate all data (mine and from the other processes) 
        free_dmatrix(OUT->Vxglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        free_dmatrix(OUT->Vyglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        free_dmatrix(OUT->Vzglobal, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
  } /*end if I'm the writer rank, wrote the surfacexz file*/

  /* ======================================================================================================= */
  /* Writing of surfaceyz */
  /* ======================================================================================================= */
  //Look for the writer rank, the process with i0 and coords[1] = 0, we know its coordinates (icpu, 0), so rank = icpu 
  icpu = PRM->i2icpu_array[OUT->i0];
  imp_tmp =  PRM->i2imp_array[OUT->i0];

  if ( PRM->me  == icpu ){ //if I'm the writer rank
	//allocate data structure to keep all data from myself and the other processes
        OUT->Vxglobal = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        OUT->Vyglobal = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        OUT->Vzglobal = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);

	//copy my data to the data structure
        for ( j = 1; j <= PRM->mpmy; j++){
          for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
            if ( outvel == 1){
              OUT->Vxglobal[PRM->yMin-PRM->delta+j-1][k] = v0->x[imp_tmp][j][k];
              OUT->Vyglobal[PRM->yMin-PRM->delta+j-1][k] = v0->y[imp_tmp][j][k];
              OUT->Vzglobal[PRM->yMin-PRM->delta+j-1][k] = v0->z[imp_tmp][j][k];
            }else  if ( outdisp == 1 ){
              OUT->Vxglobal[PRM->yMin-PRM->delta+j-1][k] = OUT->Uyz[1][j][k];
              OUT->Vyglobal[PRM->yMin-PRM->delta+j-1][k] = OUT->Uyz[2][j][k];
              OUT->Vzglobal[PRM->yMin-PRM->delta+j-1][k] = OUT->Uyz[3][j][k];
            }
          }
        }
  } /* if I'm the writer rank */

  for ( i1= 1; i1 <= 4; i1++){ //we do it in four steps
	if ( (PRM->coords[0] == icpu) && (PRM->me != icpu) ){ //if I'm involved in this file (not all processes are), but I'm not the writer rank, so I'll send my data to the writer rank in four steps
	        if ( i1 == 1 ){ //step 1: send coordinates
		        MPI_Isend (PRM->coords,2,MPI_INT,icpu,90,comm2d,&sendreq[0]);
		        MPI_Wait (&sendreq[0], &status);
	        } /* end of step i1 = 1 */
	        if ( i1 == 2 ){ //step 2: send my x data
		        imp = 0;
		        for ( j = 1; j <= PRM->mpmy; j++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->x[imp_tmp][j][k];
			                } else  if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uyz[1][j][k];
			                }
		                } /*end for k */
		         } /*end for j*/
            		 MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,icpu,80,comm2d,&sendreq[1]);
		         MPI_Wait (&sendreq[1], &status);
		} /* end of step i1 = 2 */
	        if ( i1 == 3 ){ //step 3: send my y data
		         imp = 0;
		         for ( j = 1; j <= PRM->mpmy; j++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size);
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->y[imp_tmp][j][k];
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uyz[2][j][k];
			                }
			         } /*end for k*/
		         } /*end for j*/
		         MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,icpu,81,comm2d,&sendreq[2]);
		         MPI_Wait (&sendreq[2], &status);
	        } /* end of step i1 = 3 */
	        if ( i1 == 4 ){ //step 4: send my z data
		        imp = 0;
		        for ( j = 1; j <= PRM->mpmy; j++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size);
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->z[imp_tmp][j][k];
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uyz[3][j][k];
			                }
			        } /*end for k */
		        } /*end for j */
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,icpu,82,comm2d,&sendreq[3]);
		        MPI_Wait (&sendreq[3], &status);
          	} /* end of step i1 = 4 */
        } /* end of if I'm involved but I'm not the writer rank */
        if ( PRM->me == icpu ){ //if I'm the writer rank, I'll receive data from others
	        for ( i2 = 1; i2 < PRM->py ; i2++){ //do the four steps to all other processes involved in the file
            		if ( i1 == 1 ){ //step 1: receive coordinates
              			MPI_Recv (proc_coords,2,MPI_INT,icpu+PRM->px*i2,90,comm2d,&status);
		                coords_global[0][i2] = proc_coords[0];
			        coords_global[1][i2] = proc_coords[1];
            		} /* end of step i1 = 1 */
            		if ( i1 == 2 ){ //step 2: receive x data
			        MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,80, comm2d,&status);
		                OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
			        for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                OUT->Vxglobal[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			                 } /*end for k*/
		                 } /*end for j*/
		        } /* end of step i1 = 2 */
	                if ( i1 == 3 ){ //step 3: receive y data
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,81, comm2d,&status);
		                OUT->total_prec_y = 0;
		                for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                OUT->Vyglobal[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			                 } /*end for k */
		                } /*end for j */
		        } /* end of step i1 = 3 */
		        if ( i1 == 4 ){ //step 4: receive z data
	  	              MPI_Recv(OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,82, comm2d,&status);
		              OUT->total_prec_y = 0;
		              for ( j = 0; j < coords_global[1][i2]; j++){
			              OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		              }
		              imp = 0;
		              for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			              for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				              assert (imp < OUT->test_size) ;
				              assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				              imp ++;
				              OUT->Vzglobal[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			               } /*end for k */
		               } /*end for j*/
		        } /* end of step i1 = 4 */
		} /* end of for all other processes involved in this file */
	} /* end of if I'm the writer rank */
  } /* end of the four steps */

  //now write the file
  if ( PRM->me == icpu ) { //if I'm the writer rank
        int ndiv =1;
        double dssurf=PRM->ds*ndiv;

        int YMINS=(int) ceil((PRM->yMin-PRM->delta)/ndiv);
        int YMAXS=(int) floor((PRM->yMax+PRM->delta)/ndiv);
        int dimy= YMAXS-YMINS+1;
        int ZMINS=(int) ceil((PRM->zMin-PRM->delta)/ndiv);
        int ZMAXS=(int) floor((1)/ndiv);
        int dimz= ZMAXS-ZMINS+1;

	flname[0] = '\0';
	sprintf(flname, "%ssurfaceyz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp3 = fopen(flname, "w");

        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",1,dimy,dimz);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "ORIGIN %f %f %f\n",OUT->i0*PRM->ds,YMINS*dssurf,ZMINS*dssurf);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",dimy*dimz*1);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));

        for ( k = PRM->zMin-PRM->delta+1; k <= 2; k++ ){
          for ( j = PRM->yMin-PRM->delta+1; j <= PRM->yMax+PRM->delta+1; j++ ){
            if( ((j-1)%ndiv) == 0 && ((k-1)%ndiv) == 0 ){
              write_float(fp3,(float) OUT->Vxglobal[j][k], write_buffer);
              write_float(fp3,(float) OUT->Vyglobal[j][k], write_buffer);
              write_float(fp3,(float) OUT->Vzglobal[j][k], write_buffer);
            }
          }
        }
	flush_buffer_to_file(fp3, write_buffer);
        fclose(fp3);
	
	//free buffer allocated for this file
        free_dmatrix(OUT->Vxglobal, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        free_dmatrix(OUT->Vyglobal, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        free_dmatrix(OUT->Vzglobal, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
  } /*end if I'm the writer rank, wrote the surface yz file */
}
//------------------------------------------------------------------------------------------------------------
void write_surface_files_mpiio(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int x, int y, int np, char *mpiio_surface_buffer, MPI_Comm comm2d, int l)
{
  FILE *fp1, *fp2, *fp3, *fp5;
  int header_size = 0;
  MPI_Status status;
  MPI_Request sendreq[0];
  MPI_File fh;
  int err, resultlen, jmp_tmp, z_size, icpu, imp_tmp;
  char msg[256];
  int first_i, first_j, max_i, max_j;
  float val1, val2, val3;
  int i, j, index;
  int offset_diff;
  int starting_offset;
  MPI_Datatype my_datatype;
  MPI_Comm comm_vtk_xz;
  int three_floats = 3*sizeof(float);
  int one_float = sizeof(float);
  int two_floats = 2*sizeof(float);
	int jcpu, color, new_rank;
		
        int ndiv =1;
        double dssurf=PRM->ds*ndiv;
        int XMINS=(int) ceil((PRM->xMin-PRM->delta)/ndiv);
        int XMAXS=(int) floor((PRM->xMax+PRM->delta)/ndiv);
        int YMINS=(int) ceil((PRM->yMin-PRM->delta)/ndiv);
        int YMAXS=(int) floor((PRM->yMax+PRM->delta)/ndiv);
        int ZMINS=(int) ceil((PRM->zMin-PRM->delta)/ndiv);
        int ZMAXS=(int) floor((1)/ndiv);
        int dimz= ZMAXS-ZMINS+1;
  int dimx= XMAXS-XMINS+1;
        int dimy= YMAXS-YMINS+1;
char flname[80];
  //******************************************************************************************************
  /* Writing of surfacexy */
  //******************************************************************************************************
  //open the file
	flname[0] = '\0';
	sprintf(flname, "%ssurfacexy%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
  err = MPI_File_open(comm2d, flname, MPI_MODE_WRONLY | MPI_MODE_CREATE ,MPI_INFO_NULL,&fh);
  if(err != MPI_SUCCESS)
  {
	MPI_Error_string(err, msg, &resultlen);
	fprintf(stderr, "Error: could not open the file %s, aborting output routine\n%s\n", flname, msg);
	return;
  }
  //rank 0 will write the header
  if(PRM->me == 0)
  {
        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\nDIMENSIONS %d %d %d\nORIGIN %f %f %f\nSPACING %f %f %f\nPOINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",dimx,dimy,1,XMINS*dssurf,YMINS*dssurf,0.,dssurf,dssurf,dssurf, dimx*dimy*1);
	header_size = strlen(*local_buf);
//		printf("rank %d will write the header\n", PRM->me);
	err = MPI_File_write_at(fh, 0, *local_buf, header_size, MPI_CHAR, MPI_STATUS_IGNORE);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not write header to file %s, aborting surface output routine\n%s\n", flname, msg);
		MPI_File_close(&fh);
		return;
	}	
  } //end if I'm rank 0, which wrote the header of the file
  //now everyone needs to know the header size
  MPI_Bcast(&header_size, 1, MPI_INT, 0, comm2d);
  //now everyone will write to the file
  //make adjustments to the amount of data border process will write 
  //the original code does not write the first and the last line and column
  //I don't know why it is done like this, but I wanted to generate the exact same output, so..
  setup_write_vtk_indexes(PRM, x, y, PRM->mpmx, PRM->mpmy, &first_i, &first_j, &max_i, &max_j);
  index=0;
	
  //put all my data in the buffer
  //data is written in column order
  for(j=first_j; j<max_j; j++)
  {
	for(i=first_i; i< max_i; i++)
	{
		if( outvel == 1)
		{
			if(surface == ABSORBING || model == GEOLOGICAL)
			{
				val1 = v0->x[i+1][j+1][OUT->k0];
				val2 = v0->y[i+1][j+1][OUT->k0];
				val3 = v0->z[i+1][j+1][OUT->k0-1];
			}
			else //surface == FREE && model != GEOLOGICAL
			{
				val1 = v0->x[i+1][j+1][1];
				val2 = v0->y[i+1][j+1][1];
				val3 = v0->z[i+1][j+1][0];
			}
		}
		else //outdisp
		{
			val1 = OUT->Uxy[1][i+1][j+1];
			val2 = OUT->Uxy[2][i+1][j+1];
			val3 = OUT->Uxy[3][i+1][j+1];
		}
		force_big_endian((unsigned char *)&val1);
		force_big_endian((unsigned char *)&val2);
		force_big_endian((unsigned char *)&val3);
		mpiio_surface_buffer[index*three_floats] = val1;
		mpiio_surface_buffer[index*three_floats + one_float] = val2;
		mpiio_surface_buffer[index*three_floats + two_floats] = val3;
		index++;
	} //end for i
  } //end for j
  //create a datatype to describe each contiguous portion of data and the distance between them
  //here the contiguous portion is a column of our matrix
  offset_diff = 0;
  for(i=0; i< PRM->px; i++)
	offset_diff += PRM->mpmx_tab[i]*3;
  if(!Create_new_output_datatype((max_j - first_j), (max_i - first_i)*3, offset_diff, &my_datatype, MPI_FLOAT))
  {
	MPI_File_close(&fh);
	fprintf(stderr, "Error: could not create MPI-IO datatype to write surface files, aborting surface output routine\n");
	return;
  }
  //define a file view which is formed by the defined datatype, starting at a given offset
  starting_offset = 0;
  for(j=0; j < y; j++)
	for(i=0; i< PRM->px; i++)
		starting_offset += PRM->mpmx_tab[i]*PRM->mpmy_tab[j]*three_floats;
  for(i = 0; i< x; i++)
	starting_offset += PRM->mpmx_tab[i]*three_floats;
  err = MPI_File_set_view(fh, header_size+starting_offset, MPI_FLOAT, my_datatype, "native", MPI_INFO_NULL);
  if(err != MPI_SUCCESS)
  {
	MPI_Error_string(err, msg, &resultlen);
	fprintf(stderr, "Error: could not create MPI-IO file view for surface file, aborting surface output routine\n%s\n", msg);
	MPI_File_close(&fh);
	MPI_Type_free(&my_datatype);
	return;
  }
  //write all data at once
  err = MPI_File_write_all(fh, mpiio_surface_buffer, (max_i - first_i)*(max_j - first_j)*3, MPI_FLOAT, MPI_STATUS_IGNORE);
  if(err != MPI_SUCCESS)
  {
	MPI_Error_string(err, msg, &resultlen);
	fprintf(stderr, "Error: could not write to file %s, aborting surface output routine\n%s\n", flname, msg);
	return;
  }
  //cleanup datatype, close file, restore mpmx_tab and mpmy_tab
  MPI_File_close(&fh);
  MPI_Type_free(&my_datatype);
  restore_write_vtk_indexes(PRM);

//******************************************************************************************************
/* Writing of surfacexz */
//******************************************************************************************************
  jcpu = PRM->j2jcpu_array[OUT->j0];
  //create a new communicator to be used by processes who will write surfacexz file
  color = (PRM->coords[1] == jcpu) ? 1 : 0;
  err = MPI_Comm_split(comm2d, color, PRM->me, &comm_vtk_xz);
  if(err != MPI_SUCCESS)
  {
 	MPI_Error_string(err, msg, &resultlen);
	fprintf(stderr, "Error: could not split the MPI communicator to write xz surface file, aborting surface output routine\n%s\n", msg);
	return;
  }		
  if(color == 1) //if I'm involved in this file
  { //only the processes that will participate in writing file xz enter here
	MPI_Comm_rank(comm_vtk_xz, &new_rank);
	//open the file
	flname[0] = '\0';
	sprintf(flname, "%ssurfacexz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
	err = MPI_File_open(comm_vtk_xz, flname, MPI_MODE_WRONLY | MPI_MODE_CREATE ,MPI_INFO_NULL,&fh);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not open the file %s, aborting surface output routine\n%s\n", flname, msg);
		return;
	}
	header_size = 0;
	//rank 0 will write the header
	if(new_rank == 0)
	{
	       	/* print VTK header*/
		(*local_buf)[0]='\0';
		sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\nDIMENSIONS %d %d %d\nORIGIN %f %f %f\nSPACING %f %f %f\nPOINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",dimx,1,dimz,XMINS*dssurf,OUT->j0*PRM->ds,ZMINS*dssurf,dssurf,dssurf,dssurf,dimx*dimz*1);
		header_size = strlen(*local_buf);
	//	pwrite(fd, local_buf, header_size, 0);
//		printf("rank %d will write the header\n", PRM->me);
		err = MPI_File_write_at(fh, 0, *local_buf, header_size, MPI_CHAR, MPI_STATUS_IGNORE);
		if(err != MPI_SUCCESS)
		{
			MPI_Error_string(err, msg, &resultlen);
			fprintf(stderr, "Error: could not write header to file %s, aborting surface output routine\n%s\n", flname, msg);
			MPI_File_close(&fh);
			return;
		}
	}
	//now everyone needs to know the header size
	MPI_Bcast(&header_size, 1, MPI_INT, 0, comm_vtk_xz);
 	//now everyone will write to the file
	//we cut the first and last columns
	setup_write_vtk_indexes(PRM, x, y, PRM->mpmx, PRM->mpmy, &first_i, &first_j, &max_i, &max_j);
	index=0;
	jmp_tmp =  PRM->j2jmp_array[OUT->j0];
	//put all data in the buffer
	int max_z = 2; //in the original code, each process would transmit its portion between PRM->zMin-PRM->delta and PRM->zMax0 to the writer rank, but the writer rank will write only between PRM->zMin-PRM->delta+1 and 2. So here I'm writing only data which will be used. 
	z_size = (max_z - (PRM->zMin-PRM->delta))+1;
	//data is written in column order
	for(j = 1; j< z_size; j++) //we cut the first line
	{
		for(i=first_i; i< max_i; i++)
		{
			if( outvel == 1)
			{
				val1 = v0->x[i+1][jmp_tmp][PRM->zMin-PRM->delta+j];
				val2 = v0->y[i+1][jmp_tmp][PRM->zMin-PRM->delta+j];
				val3 = v0->z[i+1][jmp_tmp][PRM->zMin-PRM->delta+j];
			}
			else //outdisp
			{
				val1 = OUT->Uxz[1][i+1][PRM->zMin-PRM->delta+j];
				val2 = OUT->Uxz[2][i+1][PRM->zMin-PRM->delta+j];
				val3 = OUT->Uxz[3][i+1][PRM->zMin-PRM->delta+j];
			}
			force_big_endian((unsigned char *)&val1);
			force_big_endian((unsigned char *)&val2);
			force_big_endian((unsigned char *)&val3);
			mpiio_surface_buffer[index*three_floats] = val1;
			mpiio_surface_buffer[index*three_floats + one_float] = val2;
			mpiio_surface_buffer[index*three_floats + two_floats] = val3;
			index++;
		} //end for i
	} //end for j

	//create a datatype to describe each contiguous portion of data and the distance between them
	//here the contiguous portion is a column of our matrix
	offset_diff = 0;
	for(i=0; i< PRM->px; i++)
		offset_diff += PRM->mpmx_tab[i]*3;
	if(!Create_new_output_datatype((z_size-1), (max_i - first_i)*3, offset_diff, &my_datatype, MPI_FLOAT))
	{
		fprintf(stderr, "Error: could not create new MPI-IO datatype for surface output\n");
		MPI_File_close(&fh);
		return;
	}
	//define a file view which is formed by the defined datatype, starting at a given offset
	starting_offset = 0;
	for(i=0; i< x; i++)
		starting_offset += PRM->mpmx_tab[i]*three_floats;
	err = MPI_File_set_view(fh, header_size+starting_offset, MPI_FLOAT, my_datatype, "native", MPI_INFO_NULL);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not create file view\n%s\n", msg);
		MPI_File_close(&fh);
		MPI_Type_free(&my_datatype);
		return;
	}
	//write all data at once
	err = MPI_File_write_all(fh, mpiio_surface_buffer, (max_i - first_i)*(z_size-1)*3, MPI_FLOAT, MPI_STATUS_IGNORE);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not write to file %s\n%s\n", flname, msg);
		return;
	}
	//cleanup datatype, close file, restore mpmx_tab and mpmy_tab
	MPI_File_close(&fh);
	MPI_Type_free(&my_datatype);
	restore_write_vtk_indexes(PRM);
  } //end if I'm involved in this file
  //clean up the communicator we've created to write file xz
  MPI_Comm_free(&comm_vtk_xz);

//******************************************************************************************************
/* Writing of surfaceyz */
//******************************************************************************************************
  icpu = PRM->i2icpu_array[OUT->i0];
  //create a new communicator to be used by processes who will write surfaceyz file
  color = (PRM->coords[0] == icpu) ? 1: 0;
  MPI_Comm comm_vtk_yz;
  err = MPI_Comm_split(comm2d, color, PRM->me, &comm_vtk_yz);
  if(err != MPI_SUCCESS)
  {
	MPI_Error_string(err, msg, &resultlen);
	fprintf(stderr, "Error: could not split the communicator to write YZ vtk file!\n%s\n", msg);
	return;
  }	
  if(color == 1) //if I'm involved in this file 
  {//only the processes that will participate in writing file yz enter here
	MPI_Comm_rank(comm_vtk_yz, &new_rank);
	//open the file
	flname[0] = '\0';
	sprintf(flname, "%ssurfaceyz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
//		printf("rank %d will open the file %s\n", PRM->me, flname3);
	err = MPI_File_open(comm_vtk_yz, flname, MPI_MODE_WRONLY | MPI_MODE_CREATE ,MPI_INFO_NULL,&fh);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not open the file %s\n%s\n", flname, msg);
		return;
	}
	header_size = 0;
	//rank 0 will write the header
	if(new_rank == 0)
	{
        	/* print VTK header*/
		(*local_buf)[0]='\0';
		sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\nDIMENSIONS %d %d %d\nORIGIN %f %f %f\nSPACING %f %f %f\nPOINT_DATA %d\nSCALARS V float 3\nLOOKUP_TABLE default\n",1,dimy,dimz,OUT->i0*PRM->ds,YMINS*dssurf,ZMINS*dssurf,dssurf,dssurf,dssurf,dimy*dimz*1);
		header_size = strlen(*local_buf);
		//pwrite(fd, *local_buf, header_size, 0);
//			printf("rank %d will write the header\n", PRM->me);
		err = MPI_File_write_at(fh, 0, *local_buf, header_size, MPI_CHAR, MPI_STATUS_IGNORE);
		if(err != MPI_SUCCESS)
		{
			MPI_Error_string(err, msg, &resultlen);
			fprintf(stderr, "Error: could not write header to file %s\n%s\n", flname, msg);
			MPI_File_close(&fh);
			return;
		}
	} //end if I'm the rank 0 among the ones involved in this file
	//now everyone needs to know the header size
	MPI_Bcast(&header_size, 1, MPI_INT, 0, comm_vtk_yz);
	//now everyone will write to the file
	//we don't output the first and last lines
	setup_write_vtk_indexes(PRM, x, y, PRM->mpmx, PRM->mpmy, &first_i, &first_j, &max_i, &max_j);
	index=0;
	imp_tmp =  PRM->i2imp_array[OUT->i0];
	//put all data in the buffer
	int max_z = 2; //in the original code, each process would transmit its portion between PRM->zMin-PRM->delta and PRM->zMax0 to the writer rank, but the writer rank will write only between PRM->zMin-PRM->delta+1 and 2. So here I'm writing only data which will be used. 
	z_size = (max_z - (PRM->zMin-PRM->delta))+1;
	//data is written in column order
	for(i = 1; i< z_size; i++)
	{
		for(j=first_j; j< max_j; j++)
		{
			if(outvel == 1)
			{
				val1 = v0->x[imp_tmp][j+1][i+1];
				val2 = v0->y[imp_tmp][j+1][i+1];
				val3 = v0->z[imp_tmp][j+1][i+1];
			}
			else
			{
				val1 = OUT->Uyz[1][j+1][i+1];
				val2 = OUT->Uyz[2][j+1][i+1];
				val3 = OUT->Uyz[3][j+1][i+1];
			}
			force_big_endian((unsigned char *)&val1);
			force_big_endian((unsigned char *)&val2);
			force_big_endian((unsigned char *)&val3);
			mpiio_surface_buffer[index*three_floats] = val1;
			mpiio_surface_buffer[index*three_floats + one_float] = val2;
			mpiio_surface_buffer[index*three_floats + two_floats] = val3;
			index++;
		} //end for j
	} //end for i
	//create a datatype to describe each contiguous portion of data and the distance between them
	//here the contiguous portion is a column of our matrix
	offset_diff = 0;
	for(i=0; i< PRM->py; i++)
		offset_diff += PRM->mpmy_tab[i]*3;
	if(!Create_new_output_datatype((z_size-1), (max_j - first_j)*3, offset_diff, &my_datatype, MPI_FLOAT))
	{
		MPI_File_close(&fh);
		fprintf(stderr, "Error! Could not create MPI-IO datatype to write surface file\n");
		return;
	}
	//define a file view which is formed by the defined datatype, starting at a given offset
	starting_offset = 0;
	for(i=0; i< y; i++)
		starting_offset += PRM->mpmy_tab[i]*three_floats;
	err = MPI_File_set_view(fh, header_size+starting_offset, MPI_FLOAT, my_datatype, "native", MPI_INFO_NULL);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "Error: could not create file view\n%s\n", msg);
		MPI_File_close(&fh);
		MPI_Type_free(&my_datatype);
		return;
	}
	//write all data at once
	err = MPI_File_write_all(fh, mpiio_surface_buffer, (max_j - first_j)*(z_size-1)*3, MPI_FLOAT, MPI_STATUS_IGNORE);
	if(err != MPI_SUCCESS)
	{
		MPI_Error_string(err, msg, &resultlen);
		fprintf(stderr, "PANIC! Could not write to file %s\n%s\n", flname, msg);
		return;
	}
	//cleanup datatype, close file, restore mpmx_tab and mpmy_tab
	MPI_File_close(&fh);
	MPI_Type_free(&my_datatype);
	restore_write_vtk_indexes(PRM);
  } //end if I'm involved in this file
	
  //clean up the communicator we've created to write file yz
  MPI_Comm_free(&comm_vtk_yz);
}
//------------------------------------------------------------------------------------------------------------
//in the paper from PDP 2017, this implementation is referred to as "distributed surface output"
void write_surface_files_parallel_communicationstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, int writer_xy, int writer_xz, int writer_yz, struct xyzbuf_t *xyzbuf, int np, int **coords_global, int l, MPI_Comm comm2d)
{
  int i, j, k, i1, i2, imp, jmp_tmp, jcpu, imp_tmp, icpu;
  MPI_Request sendreq[4];
  MPI_Status status;

if(PRM->me == 0)
	fprintf(stderr, "start surface communication step\n");

  //optimization 1: we'll separate communication from the writing of the file because otherwise a process which is busy writing a file will retard the whole communication step of the next file
  //In other words, first we'll exchange all data, and only then the writer ranks will output to files
  //===================COMMUNICATION STEP OF SURFACEXY==============================
  if ( PRM->me == writer_xy){ //If I'm the one writing this file
	if(l == PRM->surfout) //this is the first surface output phase, we have to allocate buffers
	{
	        xyzbuf->xx = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        	xyzbuf->xy = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        	xyzbuf->xz = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
	}
	OUT->total_prec_x = 0;
	for(j = 0; j < PRM->coords[0]; j++) {
                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
        }
        OUT->total_prec_y = 0;
        for ( j = 0; j < PRM->coords[1]; j++){
                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
        }
	//copy the local data to the global buffer
        for ( i = 1; i <= PRM->mpmx; i++){
        	for ( j = 1; j <= PRM->mpmy; j++){	
		        if ( outvel == 1){
              			if ( surface == ABSORBING || model == GEOLOGICAL ){
			                xyzbuf->xx[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->x[i][j][OUT->k0];
			                xyzbuf->xy[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->y[i][j][OUT->k0];
			                xyzbuf->xz[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->z[i][j][OUT->k0 -1 ];
			        } else if ( surface == FREE && model != GEOLOGICAL){ /* free surface at z = 0 */
			                xyzbuf->xx[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->x[i][j][1];
			                xyzbuf->xy[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->y[i][j][1];
			                xyzbuf->xz[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = v0->z[i][j][0];
		                } /* end of if surface */
			} else  if ( outdisp == 1 ){
				xyzbuf->xx[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = OUT->Uxy[1][i][j];
		                xyzbuf->xy[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = OUT->Uxy[2][i][j];
		                xyzbuf->xz[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][PRM->yMin-PRM->delta+j-1+OUT->total_prec_y] = OUT->Uxy[3][i][j];
		        } /* end if snapType is velocity or displacement */
		} /* end for j */
	} /* end for i */
  } /* end if I'm the writer of this file, memory was allocated and I copied my local data to the buffer */
  //now all processes will send their data to process 0 in 3 messages, one for x, another for y, and a third for z
  for ( i1 = 2; i1 <= 4; i1++){
	if ( PRM->me != writer_xy ){ //if I'm not the writer of this file, I'll send data to the writer
		if ( i1 == 2 ){ 
            		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
			                if ( surface == ABSORBING ){ OUT->snapBuff[imp] = v0->x[i][j][OUT->k0];}
			                else if ( surface == FREE ){OUT->snapBuff[imp] = v0->x[i][j][1];}
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxy[1][i][j];
                			}
			        } /* end for j */
		        } /* end for i */
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xy,80,comm2d,&sendreq[1]);
		        MPI_Wait (&sendreq[1], &status);
		} /* end of i1 = 2 */
	        if ( i1 == 3 ){
            		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                if ( surface == ABSORBING ){ OUT->snapBuff[imp] = v0->y[i][j][OUT->k0];}
				                else if ( surface == FREE ){OUT->snapBuff[imp] = v0->y[i][j][1];}
				                } else if ( outdisp == 1 ){
					                OUT->snapBuff[imp] = OUT->Uxy[2][i][j];
				                }
			        } /* end for j */
			} /* end for i */
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xy,81,comm2d,&sendreq[2]);
		        MPI_Wait (&sendreq[2], &status);
	        } /* end of i1 = 3 */
		if ( i1 == 4 ){
		        imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( j = 1; j <= PRM->mpmy; j++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                if ( surface == ABSORBING ){ /* absorbing layer above z = PRM->zMax */
					                OUT->snapBuff[imp] = v0->z[i][j][OUT->k0];
				                } else if ( surface == 1 ){ /* free surface at z = 0 */
					                OUT->snapBuff[imp] = v0->z[i][j][0];
				                } /* end of if surface */
					} else  if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxy[3][i][j];
			                }
		                } /* end for j */
		        } /* end for i*/
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xy,82,comm2d,&sendreq[3]);
		        MPI_Wait (&sendreq[3], &status);
		} /* end of i1 = 4 */
        } else { //if I'm the writer of this file, I need to receive data
		//rank 0 will receive three messages from each process
	        for ( i2 = 0; i2 < np; i2++){
			if(i2 == PRM->me)
				continue;
		        if ( i1 == 2 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,80, comm2d,&status);
              			OUT->total_prec_x = 0;
		                for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x += PRM->mpmx_tab[j];
		                }
		                OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y += PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->xx[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
		                } /* end for i */
			} /* end of i1 = 2 */
		        if ( i1 == 3 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,81, comm2d,&status);
              			OUT->total_prec_x = 0;
			        for ( j = 0; j < coords_global[0][i2];j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
			        OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->xy[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
		                } /* end for i */
	                } /* end of i1 = 3 */
	                if ( i1 == 4 ){
              	      		MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, i2,82, comm2d,&status);
		                OUT->total_prec_x = 0;
			        for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
		                OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
			        for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->xz[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][PRM->yMin-PRM->delta-1+j+OUT->total_prec_y] = OUT->snapBuff[imp];
			                } /* end for j */
		                } /* end for i */
	              } /* end of i1 = 4 */
		} /* end for all other processes */
	} /* end if I'm the writer rank, received all data from other processes */
  } /* end of the three communication steps */
  //==============END OF COMMUNICATION STEP OF SURFACEXY==============================

if(PRM->me == 0)
	fprintf(stderr, "end surface communication step - XY\n");
  //===================COMMUNICATION STEP OF SURFACEXZ==============================
  jmp_tmp =  PRM->j2jmp_array[OUT->j0];
  jcpu = PRM->j2jcpu_array[OUT->j0];

  if ( PRM->me  == writer_xz ){ //jcpu*PRM->px //the writer rank for this file
	if(l == PRM->surfout) //first surface output phase
	{
	        xyzbuf->zx = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        	xyzbuf->zy = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	        xyzbuf->zz = dmatrix(PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	}
	//copy local data to the buffer
	OUT->total_prec_x = 0;
        for ( j = 0; j < PRM->coords[0]; j++){
        	OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
        }
        for ( i = 1; i <= PRM->mpmx; i++){
        	for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
            		if ( outvel == 1){
			        xyzbuf->zx[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = v0->x[i][jmp_tmp][k];
		                xyzbuf->zy[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = v0->y[i][jmp_tmp][k];
		                xyzbuf->zz[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = v0->z[i][jmp_tmp][k];
		        } else  if ( outdisp == 1 ){
				xyzbuf->zx[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = OUT->Uxz[1][i][k];
		                xyzbuf->zy[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = OUT->Uxz[2][i][k];
		                xyzbuf->zz[PRM->xMin-PRM->delta+i-1+OUT->total_prec_x][k] = OUT->Uxz[3][i][k];
			}
		} //end for k
	} //end for i
  } //end if I'm the writer rank, allocated memory and copied local data into the buffer
  //communication will happen in three steps
  for ( i1 = 2; i1 <=4; i1++){
	if ( (PRM->coords[1] == jcpu) && (PRM->me != writer_xz) ){ //if I'm involved in this file but I'm not the writer rank, I'll send three messages to the writer with the data to be written
		if ( i1 == 2 ){
			imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size);
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->x[i][jmp_tmp][k];
			                } else  if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxz[1][i][k];
			                }
		                } //end for k
            		} //end for i
            		MPI_Isend(OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xz,80,comm2d,&sendreq[1]);
		        MPI_Wait (&sendreq[1], &status) ;
		} /* end of i1 = 2 */
	        if ( i1 == 3 ){
        		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
	  	              for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			              assert (imp < OUT->test_size);
			              imp ++;
			              if ( outvel == 1){
				              OUT->snapBuff[imp] = v0->y[i][jmp_tmp][k];
			              } else  if ( outdisp == 1 ){
				              OUT->snapBuff[imp] = OUT->Uxz[2][i][k];
			              }
		               } //end for k
            		} //end for i
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xz,81,comm2d,&sendreq[2]);
		        MPI_Wait (&sendreq[2], &status);
          	} /* end of i1 = 3 */
	        if ( i1 == 4 ){
        		imp = 0;
		        for ( i = 1; i <= PRM->mpmx; i++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->z[i][jmp_tmp][k];
			                } else  if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uxz[3][i][k];
			                }
              			} //end for k
            		} //end for i
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_xz,82,comm2d,&sendreq[3]);
		        MPI_Wait (&sendreq[3], &status);
          	} /* end of i1 = 4 */
        } /* end if I'm involved in this file but I'm not the writer */
        else if ( PRM->me == writer_xz ){ //if I'm the writer of this file
		for ( i2 = 0; i2 < PRM->px ; i2++){ //repeat the three steps for each other processes involved in this file
			if(i2 == PRM->coords[0]) 
			{
				continue;
			}
		        if ( i1 == 2 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,80, comm2d,&status);
              			OUT->total_prec_x = 0;
		                for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->zx[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
			                } //end for k
		                } //end for i
			} /* end of i1 = 2 */
	                if ( i1 == 3 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,81, comm2d,&status);
              			OUT->total_prec_x = 0;
			        for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
			        imp = 0;
			        for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->zy[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
			                }
			        }
			} /* end of i1 = 3 */
		        if ( i1 == 4 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, jcpu*PRM->px+i2,82, comm2d,&status);
		                OUT->total_prec_x = 0;
			        for ( j = 0; j < coords_global[0][i2]; j++){
			                OUT->total_prec_x = OUT->total_prec_x + PRM->mpmx_tab[j];
		                }
		                imp = 0;
		                for ( i = 1; i <= PRM->mpmx_tab[coords_global[0][i2]]; i++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->xMin-PRM->delta-1+i+OUT->total_prec_x < PRM->xMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->zz[PRM->xMin-PRM->delta-1+i+OUT->total_prec_x][k] = OUT->snapBuff[imp];
			                } //end for k
              			} //end for i
			} /* end of i1 = 4 */
		} /* end for all other processes involved in this file*/
	} /* end if I'm the writer rank, received all data */
  } /* end of the three communication steps for this file */
  //==============END OF COMMUNICATION STEP OF SURFACEXZ==============================

if(PRM->me == writer_xz)
	fprintf(stderr, "end surface communication step - XZ\n");
  //===================COMMUNICATION STEP OF SURFACEYZ==============================
  imp_tmp =  PRM->i2imp_array[OUT->i0];
  if ( PRM->me  == writer_yz){ //if I'm the writer of this file
	if(l == PRM->surfout) //first surface output phase
	{
	        xyzbuf->yx = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        	xyzbuf->yy = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	        xyzbuf->yz = dmatrix(PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	}
	//copy local data to buffer
	OUT->total_prec_y = 0;
        for ( j = 0; j < PRM->coords[1]; j++){
                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
        }
        for ( j = 1; j <= PRM->mpmy; j++){
        	for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
            		if ( outvel == 1){
			        xyzbuf->yx[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = v0->x[imp_tmp][j][k];
		                xyzbuf->yy[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = v0->y[imp_tmp][j][k];
		                xyzbuf->yz[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = v0->z[imp_tmp][j][k];
		        } else if ( outdisp == 1 ){
		                xyzbuf->yx[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = OUT->Uyz[1][j][k];
		                xyzbuf->yy[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = OUT->Uyz[2][j][k];
			        xyzbuf->yz[PRM->yMin-PRM->delta+j-1+OUT->total_prec_y][k] = OUT->Uyz[3][j][k];
		        }
		} //end for k
        } //end for j
	fprintf(stderr, "I'm %d and I'll write file yz, already copied my data into the buffer\n", PRM->me);
  } //end if I'm the writer rank of this file, memory was allocated and I copied my data into the buffer
  icpu = PRM->i2icpu_array[OUT->i0];
  for ( i1= 2; i1 <= 4; i1++){
	if ( (PRM->coords[0] == icpu) && (PRM->me != writer_yz) ){ //if I'm involved in this file but I'm not the writer rank, I'll send data to the writer in three messages
		fprintf(stderr, "I'm %d and I have data for file xz, sending the message %d to %d\n", PRM->me, i1, writer_yz);
		if ( i1 == 2 ){
			imp = 0;
		        for ( j = 1; j <= PRM->mpmy; j++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size) ;
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->x[imp_tmp][j][k];
			                } else if ( outdisp == 1 ){
				                OUT->snapBuff[imp] = OUT->Uyz[1][j][k];
			                }
				} //end for k
            		} //end for j
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_yz,80,comm2d,&sendreq[1]);
		        MPI_Wait (&sendreq[1], &status);
          	} /* end of i1 = 2 */
	        if ( i1 == 3 ){
		        imp = 0;
		        for ( j = 1; j <= PRM->mpmy; j++){
	  	              for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			              assert (imp < OUT->test_size);
			              imp ++;
			              if ( outvel == 1){
				              OUT->snapBuff[imp] = v0->y[imp_tmp][j][k];
			              } else if ( outdisp == 1 ){
				              OUT->snapBuff[imp] = OUT->Uyz[2][j][k];
			              }
		              } //end for k
            		} //end for j
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_yz,81,comm2d,&sendreq[2]);
		        MPI_Wait (&sendreq[2], &status);
          	} /* end of i1 = 3 */
	        if ( i1 == 4 ){
		        imp = 0;
		        for ( j = 1; j <= PRM->mpmy; j++){
			        for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
			                assert (imp < OUT->test_size);
			                imp ++;
			                if ( outvel == 1){
				                OUT->snapBuff[imp] = v0->z[imp_tmp][j][k];
			                } else if ( outdisp == 1 ){
			 	               OUT->snapBuff[imp] = OUT->Uyz[3][j][k];
			                }
				} //end for k
            		} //end for j
            		MPI_Isend (OUT->snapBuff,OUT->test_size,MPI_DOUBLE,writer_yz,82,comm2d,&sendreq[3]);
		        MPI_Wait (&sendreq[3], &status);
          	} /* end of i1 = 4 */
        } /* end of if icpu && PRM->me */
        else if ( PRM->me == writer_yz){ //if I'm the writer rank, I'll receive data from other processes in three messages
		for ( i2 = 0; i2 < PRM->py ; i2++){ //for all other processes involved in this file
			if(i2 == PRM->coords[1]) //this is me, skip
				continue;
			fprintf(stderr, "I'm %d and I'm receiving the message %d from %d\n", PRM->me, i1, i2);
		        if ( i1 == 2 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,80, comm2d,&status);
              			OUT->total_prec_y = 0;
		                for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                } //end for j
		                imp = 0;
		                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->yx[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			                } //end for k
              			} //end for j
			} /* end of i1 = 2 */
		        if ( i1 == 3 ){
              			MPI_Recv (OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,81, comm2d,&status);
              			OUT->total_prec_y = 0;
			        for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
			        for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
					        assert (imp < OUT->test_size) ;
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->yy[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			                } //end for k
              			} //end for j			
			} /* end of i1 = 3 */
		        if ( i1 == 4 ){
				MPI_Recv(OUT->snapBuff,OUT->test_size, MPI_DOUBLE, icpu+PRM->px*i2,82, comm2d,&status);
		                OUT->total_prec_y = 0;
		                for ( j = 0; j < coords_global[1][i2]; j++){
			                OUT->total_prec_y = OUT->total_prec_y + PRM->mpmy_tab[j];
		                }
		                imp = 0;
		                for ( j = 1; j <= PRM->mpmy_tab[coords_global[1][i2]]; j++){
			                for ( k = PRM->zMin-PRM->delta; k <= PRM->zMax0; k++){
				                assert (imp < OUT->test_size) ;
				                assert (PRM->yMin-PRM->delta-1+j+OUT->total_prec_y < PRM->yMax+PRM->delta+3);
				                imp ++;
				                xyzbuf->yz[PRM->yMin-PRM->delta-1+j+OUT->total_prec_y][k] = OUT->snapBuff[imp];
			                } //end for k
		                } //end for j
			} /* end of i1 = 4 */
		} /* end for all other processes involved in this file */
	} /* end if I'm the writer rank */
  } /* end of communication steps for this file */
if(PRM->me == writer_yz)
	fprintf(stderr, "end surface communication step\n");
//==============END OF COMMUNICATION STEP OF SURFACEYZ==============================
} 
void write_surface_files_parallel_IOstep(struct PARAMETERS *PRM, struct OUTPUTS *OUT, struct VELOCITY *v0, int outvel, int outdisp, int surface, int model, char **local_buf, struct write_buffer_t *write_buffer, int writer_xy, int writer_xz, int writer_yz, struct xyzbuf_t *xyzbuf, int np, int l)
{
  FILE *fp1, *fp2, *fp3, *fp5;
  int ndiv =1;
  double dssurf=PRM->ds*ndiv;
  int i, j, k;

  int XMINS=(int) ceil((PRM->xMin-PRM->delta)/ndiv);
  int XMAXS=(int) floor((PRM->xMax+PRM->delta)/ndiv);
  int dimx= XMAXS-XMINS+1;
  int YMINS=(int) ceil((PRM->yMin-PRM->delta)/ndiv);
  int YMAXS=(int) floor((PRM->yMax+PRM->delta)/ndiv);
  int dimy= YMAXS-YMINS+1;
  int ZMINS=(int) ceil((PRM->zMin-PRM->delta)/ndiv);
  int ZMAXS=(int) floor((1)/ndiv);
  int dimz= ZMAXS-ZMINS+1;
char flname[80];

  //==================== I/O STEP OF SURFACEXY==============================
  if ( PRM->me == writer_xy){
	flname[0] = '\0';
	sprintf(flname, "%ssurfacexy%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp1 = fopen(flname, "w");

        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf)); 
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"V\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "BINARY\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",dimx,dimy,1);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,  "ORIGIN %f %f %f\n",XMINS*dssurf,YMINS*dssurf,0.);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\n",dimx*dimy*1);
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SCALARS V float 3\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"LOOKUP_TABLE default\n");
	buffer_to_file(fp1, write_buffer, *local_buf, strlen(*local_buf));

        for ( j = PRM->yMin-PRM->delta+1; j <= PRM->yMax+PRM->delta+1; j++ ){
	        for ( i = PRM->xMin-PRM->delta+1; i <= PRM->xMax+PRM->delta+1; i++ ){
        		if( ((i-1)%ndiv) == 0 && ((j-1)%ndiv) == 0 ){
              write_float(fp1,(float) xyzbuf->xx[i][j], write_buffer);
              write_float(fp1,(float) xyzbuf->xy[i][j], write_buffer);
              write_float(fp1,(float) xyzbuf->xz[i][j], write_buffer); 
            }
          }
        }
	flush_buffer_to_file(fp1, write_buffer); //we need to flush the buffer before closing the file
        fclose(fp1);
	//free memory allocated for this file
	if(l == PRM->tMax) //last timestep, so we have to free memory used by the buffers
	{
	        free_dmatrix(xyzbuf->xx, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
        	free_dmatrix(xyzbuf->xy, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
	        free_dmatrix(xyzbuf->xz, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2);
	}
  } /* end of if PRM->me */
	//==============END OF I/O STEP OF SURFACEXY==============================

	//==================== I/O STEP OF SURFACEXZ==============================
      if ( PRM->me == writer_xz){
	flname[0] = '\0';
	sprintf(flname, "%ssurfacexz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp2 = fopen(flname, "w");

        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "V\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "BINARY\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"DATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",dimx,1,dimz);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"ORIGIN %f %f %f\n",XMINS*dssurf,OUT->j0*PRM->ds,ZMINS*dssurf);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\n",dimx*dimz*1);
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"SCALARS V float 3\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf,"LOOKUP_TABLE default\n");
	buffer_to_file(fp2, write_buffer, *local_buf, strlen(*local_buf));

        for ( k = PRM->zMin-PRM->delta+1; k <= 2; k++ ){
          for ( i = PRM->xMin-PRM->delta+1; i <= PRM->xMax+PRM->delta+1; i++ ){
            if( ((i-1)%ndiv) == 0 && ((k-1)%ndiv) == 0 ){
              write_float(fp2,(float) xyzbuf->zx[i][k], write_buffer);
              write_float(fp2,(float) xyzbuf->zy[i][k], write_buffer);
              write_float(fp2,(float) xyzbuf->zz[i][k], write_buffer);
            }
          }
        }
	flush_buffer_to_file(fp2, write_buffer);
        fclose(fp2);
	if(l == PRM->tMax) //last output phase
	{
	        /* Desallocation */
        	free_dmatrix(xyzbuf->zx, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	        free_dmatrix(xyzbuf->zy, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        	free_dmatrix(xyzbuf->zz, PRM->xMin-PRM->delta, PRM->xMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	}
      } //end if writer_xz
	//==============END OF I/O STEP OF SURFACEXZ==============================

	//==================== I/O STEP OF SURFACEYZ==============================
      if ( PRM->me == writer_yz) {
	flname[0] = '\0';
	sprintf(flname, "%ssurfaceyz%s%4.4d%2.2d.vtk", PRM->dir, (outvel == 1) ? "vel" : "disp", l, 0);
        fp3 = fopen(flname, "w");

        /* print VTK header*/
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "# vtk DataFile Version 3.0\nV\nBINARY\nDATASET STRUCTURED_POINTS\n");
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "DIMENSIONS %d %d %d\n",1,dimy,dimz);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "ORIGIN %f %f %f\n",OUT->i0*PRM->ds,YMINS*dssurf,ZMINS*dssurf);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SPACING %f %f %f\n",dssurf,dssurf,dssurf);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "POINT_DATA %d\n",dimy*dimz*1);
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));
	(*local_buf)[0]='\0';
	sprintf(*local_buf, "SCALARS V float 3\nLOOKUP_TABLE default\n");
	buffer_to_file(fp3, write_buffer, *local_buf, strlen(*local_buf));

        for ( k = PRM->zMin-PRM->delta+1; k <= 2; k++ ){
          for ( j = PRM->yMin-PRM->delta+1; j <= PRM->yMax+PRM->delta+1; j++ ){
            if( ((j-1)%ndiv) == 0 && ((k-1)%ndiv) == 0 ){
              write_float(fp3,(float) xyzbuf->yx[j][k], write_buffer);
              write_float(fp3,(float) xyzbuf->yy[j][k], write_buffer);
              write_float(fp3,(float) xyzbuf->yz[j][k], write_buffer);
            }
          }
        }
	flush_buffer_to_file(fp3, write_buffer);
        fclose(fp3);
	if(l == PRM->tMax) //last surface output phase
	{
	        free_dmatrix(xyzbuf->yx, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
        	free_dmatrix(xyzbuf->yy, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	        free_dmatrix(xyzbuf->yz, PRM->yMin-PRM->delta, PRM->yMax+PRM->delta+2, PRM->zMin-PRM->delta, PRM->zMax0);
	}
      } //end if writer_yz
}
//------------------------------------------------------------------------------------------------------------
//for MPI-IO optimization
//make adjustments to PRM->mpmx_tab and PRM->mpmy_tab so we cut the first and the last lines and columns from the output. After using these tables to perform I/O, *MUST* call restore_write_vtk_indexes(PRM);
//it will calculate for loops indexes
void setup_write_vtk_indexes(struct PARAMETERS *PRM, int x, int y, int MPMX, int MPMY, int *first_i, int *first_j, int *max_i, int *max_j)
{
	int i;
	//cut the first and the last lines and columns from output
	PRM->mpmx_tab[0]--;
	PRM->mpmy_tab[0]--;
	PRM->mpmx_tab[PRM->px-1]--;
	PRM->mpmy_tab[PRM->py-1]--;
	*first_j = (y == 0) ? 1 : 0; //the first line and the first column are not in the output file
	*first_i = (x == 0) ? 1 : 0;
	*max_j = (y == (PRM->py-1)) ? MPMY-1 : MPMY;
	*max_i = (x == (PRM->px-1)) ? MPMX-1 : MPMX;
}
void restore_write_vtk_indexes(struct PARAMETERS *PRM)
{
	PRM->mpmx_tab[0]++;
	PRM->mpmy_tab[0]++;
	PRM->mpmx_tab[PRM->px-1]++;
	PRM->mpmy_tab[PRM->py-1]++;
}
	
void force_big_endian(unsigned char *bytes)
{
    static int doneTest = 0;
    static int shouldSwap = 0;

    /* declare an int*/
    int tmp1 = 1;

    /* the char pointer points to the tmp1 variable
    // so tmp2+1, +2, +3 points to the bytes of tmp1*/
    unsigned char *tmp2 = (unsigned char *) &tmp1;

    /* look if the endianness
    // of the machine has already been
    // tested*/
    if (!doneTest)
    {
        if (*tmp2 != 0)
            shouldSwap = 1;
        doneTest = 1;
    }

    if (shouldSwap)
    {
        unsigned char tmp;
        tmp = bytes[0];
        bytes[0] = bytes[3];
        bytes[3] = tmp;
        tmp = bytes[1];
        bytes[1] = bytes[2];
        bytes[2] = tmp;
   }
}
