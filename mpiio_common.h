/* mpiio_common.h
 * Author: Francieli Zanon Boito
 * this file provides headers for auxiliary functions for the "MPIIO" implementations of station and surface output routines 
 * they are discussed in the paper "High Performance I/O for Seismic Wave Propagation Simulations" (PDP 2017)
 * the paper is available at: https://lume.ufrgs.br/handle/10183/159549
 */

#ifndef _MPIIO_COMMON_H_
#define _MPIIO_COMMON_H_


int Create_new_output_datatype(int count, int len, int distance, MPI_Datatype *my_datatype, MPI_Datatype basic_type);

#endif
